<?php
/**
 * Displays post entry tags
 *
 * @package Billio
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php if ( has_tag() ) : ?>
<div class="entry__meta-tags">
	<?php
		$billio_tags = get_the_tags( get_the_ID() );
	if ( $billio_tags ) {
		foreach ( $billio_tags as $billio_tag ) {
			echo wp_kses_post( '<a href="' . esc_url( get_tag_link( $billio_tag->term_id ) ) . '" rel="tag" class="pills pills-default">' . $billio_tag->name . '</a>' );
		}
	}
	?>
</div>
<?php endif; ?>
