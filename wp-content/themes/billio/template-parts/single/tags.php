<?php
/**
 * Blog single tags
 *
 * @package Billio
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="uf-single-post__tags">

	<?php if ( has_tag() ) : ?>

		<?php
		$billio_tags = get_the_tags( get_the_ID() );
		foreach ( $billio_tags as $billio_tag ) {
			echo wp_kses_post( '<a href="' . esc_url( get_tag_link( $billio_tag->term_id ) ) . '" rel="tag" class="pills pills-default">' . $billio_tag->name . '</a>' );
		}
		?>

	<?php endif; ?>

</div>
