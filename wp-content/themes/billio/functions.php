<?php
/**
 * Theme functions and definitions.
 *
 * Sets up the theme and provides some helper functions
 * For more information on hooks, actions, and filters,
 * see http://codex.wordpress.org/Plugin_API
 *
 * @package Billio
 */

// Core Constants.
define( 'BILLIO_THEME_DIR', get_template_directory() );
define( 'BILLIO_THEME_URI', get_template_directory_uri() );
// Minimum required PHP version.
define( 'BILLIO_THEME_REQUIRED_PHP_VERSION', '5.2.4' );

/**
 * Set global content width
 *
 * @link https://developer.wordpress.com/themes/content-width/
 */
if ( ! isset( $content_width ) ) {
	$content_width = 900;
}

/**
 * Global variables
 */
$billio_customizer_panels   = array();
$billio_customizer_settings = array();
$billio_customizer_controls = array(
	'billio-image-selector' => 'Billio_Image_Selector_Control',
	'billio-switcher'       => 'Billio_Switcher_Control',
	'billio-input-slider'   => 'Billio_Input_Slider_Control',
	'billio-alpha-color-picker' => 'Billio_Alpha_Color_Picker_Control',
	'billio-icon-picker' => 'Billio_Icon_Picker_Control',
	'billio-link' => 'Billio_Link_Control',
);


require get_parent_theme_file_path( '/lcomunicacao/functions/functions-l.php' );


/**
 * Load all core theme function files.
 */
require get_parent_theme_file_path( '/inc/helpers.php' );
require get_parent_theme_file_path( '/inc/hooks.php' );
require get_parent_theme_file_path( '/inc/lib/class-billio-mobile-nav-walker.php' );
require get_parent_theme_file_path( '/inc/lib/webfonts.php' );
require get_parent_theme_file_path( '/inc/lib/class-tgm-plugin-activation.php' );
require get_parent_theme_file_path( '/inc/lib/class-billio-customizer-config.php' );
require get_parent_theme_file_path( '/inc/lib/class-billio-customizer-loader.php' );
require get_parent_theme_file_path( '/inc/lib/class-billio-walker-page.php' );

/**
 * Load panels, sections and settings.
 */
require get_parent_theme_file_path( '/inc/customizer/panels.php' );
require get_parent_theme_file_path( '/inc/customizer/sections.php' );
require get_parent_theme_file_path( '/inc/customizer/settings/general.php' );
require get_parent_theme_file_path( '/inc/customizer/settings/header.php' );
require get_parent_theme_file_path( '/inc/customizer/settings/colors.php' );
require get_parent_theme_file_path( '/inc/customizer/settings/footer.php' );
require get_parent_theme_file_path( '/inc/customizer/settings/default.php' );
require get_parent_theme_file_path( '/inc/customizer/settings/topbar.php' );

/**
 * Class instance init.
 */
$billio_customizer_loader = new Billio_Customizer_Loader();

add_action( 'tgmpa_register', 'billio_register_required_plugins' );

/**
 * Billio TGMPA
 */
function billio_register_required_plugins() {

	$plugins = array(
			array(
				'name'                  => esc_html__( 'WooCommerce','billio' ),
				'slug'                  => 'woocommerce',
				'required'              => false,
			),
			array(
				'name'                  => esc_html__( 'Footer Builder for Vast','billio' ),
				'slug'                  => 'vast-footer-builder',
				'required'              => false,
				'source'                => 'http://demoimporter.detheme.com/plugins/vast-footer-builder.zip',
			),
			array(
				'name'                  => esc_html__( 'KingComposer','billio' ),
				'slug'                  => 'kingcomposer',
				'required'              => false,
			),
			array(
				'name'                  => esc_html__( 'KC Pro!','billio' ),
				'slug'                  => 'kc_pro',
				'required'              => false,
				'source'                => 'http://demoimporter.detheme.com/plugins/kingcomposer_pro_plugin.zip',
			),
			array(
				'name'                  => esc_html__( 'Essential Grid','billio' ),
				'slug'                  => 'essential-grid',
				'required'              => true,
				'source'                => 'http://demoimporter.detheme.com/plugins/essential-grid.zip',
			),
			array(
				'name'                  => esc_html__( 'Slider Revolution','billio' ),
				'slug'                  => 'slider-revolution',
				'required'              => true,
				'source'                => 'http://demoimporter.detheme.com/plugins/slider-revolution.zip',
			),
			array(
				'name'                  => esc_html__( 'Billio Addon','billio' ),
				'slug'                  => 'billio-addon',
				'required'              => false,
				'source'                => 'http://demoimporter.detheme.com/billio/plugins/billio-addon.zip',
			),
			array(
				'name'                  => esc_html__( 'Vast Demo Importer','billio' ),
				'slug'                  => 'vast-demo-import',
				'required'              => false,
				'source'                => 'http://demoimporter.detheme.com/billio/plugins/vast-demo-import.zip',
			),
		);

	$config = array(
		'id'           => 'billio',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );

}

add_action('woocommerce_after_shop_loop_item', 'add_a_custom_button', 5 );
function add_a_custom_button() {
    global $product;

    // Not for variable and grouped products that doesn't have an "add to cart" button
    if( $product->is_type('variable') || $product->is_type('grouped') ) return;

    // Output the custom button linked to the product
    echo '<div style="margin-bottom:10px;">
        <a class="button custom-button" href="' . esc_attr( $product->get_permalink() ) . '">' . __('Ver produto') . '</a>
    </div>';
}


//Exclude pages from WordPress Search
if (!is_admin()) {
function wpb_search_filter($query) {
if ($query->is_search) {
$query->set('post_type', 'post');
}
return $query;
}
add_filter('pre_get_posts','wpb_search_filter');
}