<?php
function lcomunicacao_theme_stylesheets()
{

    wp_enqueue_style('lcomunicacao', get_template_directory_uri() . '/lcomunicacao/css/lcomunicacao.css', array(), '1.0');
    // wp_enqueue_style( 'owl', get_template_directory_uri() . '/lcomunicacao/css/owl.carousel.min.css', array(), '1.0' );
    // Theme stylesheet.

}

add_action('wp_enqueue_scripts', 'lcomunicacao_theme_stylesheets');


function wpb_adding_scripts()
{
    // wp_register_script('my_amazing_script', get_template_directory_uri() . '/lcomunicacao/js/script.js', array('jquery'),'1.1', true);
    // wp_enqueue_script('my_amazing_script');

    // wp_register_script('myy_amazing_script', get_template_directory_uri() . '/lcomunicacao/js/owl.carousel.min.js', array('jquery'),'1.1', true);
    // wp_enqueue_script('myy_amazing_script');
}

add_action('wp_enqueue_scripts', 'wpb_adding_scripts', 999);



// Register and load the widget
function wpb_load_widget()
{
    register_widget('wpb_widget');
}
add_action('widgets_init', 'wpb_load_widget');

// Creating the widget 
class wpb_widget extends WP_Widget
{

    function __construct()
    {
        parent::__construct(

            // Base ID of your widget
            'wpb_widget',

            // Widget name will appear in UI
            __('WPBeginner Widget', 'wpb_widget_domain'),

            // Widget description
            array('description' => __('Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain'),)
        );
    }

    // Creating widget front-end

    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);

        $category = apply_filters('widget_category', $instance['category']);

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];


        // This is where you run the code and display the output



        $args = array(
            'post_type'      => 'product',
            'posts_per_page' => -1,
            'product_cat'    => $category
        );

        $loop = new WP_Query($args);
?>
        <section class="related products woocommerce">

            <ul class="products columns-4 row">

                <?php
                while ($loop->have_posts()) : $loop->the_post();
                    global $product;


                    echo __('
                
            <li class="grid-item product post-12881 type-product status-publish has-post-thumbnail product_cat-wilco first instock shipping-taxable purchasable product-type-simple">
            <a href="' . get_permalink() . '" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
             ' . woocommerce_get_product_thumbnail() . '
                <h2 class="woocommerce-loop-product__title">' . get_the_title() . '</h2>
            </a>
            <a href="' . get_permalink() . '" data-quantity="1" class="button product_type_simple" data-product_id="12881" data-product_sku="" aria-label="Add “JCB Overhaul Kit Phosfluorescent STD” to your cart" rel="nofollow">Ver produto</a>
            </li>
    
                
                
        
    
        ', 'wpb_widget_domain');

                endwhile;
                ?>
            </ul>
        </section>


    <?php

        wp_reset_query();



        echo $args['after_widget'];
    }

    // Widget Backend 
    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }
        // Widget admin form
    ?>
        <p>
            <select name="category">
                <?php

                $categories = get_terms(['taxonomy' => 'product_cat']);

                // Getting a visual raw output
                foreach ($categories as $categoria) {
                    echo "<option value=" . $categoria->slug . ">" . $categoria->name . "</option>";
                }

                ?>
            </select>


            </select>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('teste:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
    <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
} // Class wpb_widget ends here















// Register and load the widget
function wpbe_load_widget()
{
    register_widget('wpbe_widget');
}
add_action('widgets_init', 'wpbe_load_widget');

// Creating the widget 
class wpbe_widget extends WP_Widget
{

    function __construct()
    {
        parent::__construct(

            // Base ID of your widget
            'wpbe_widget',

            // Widget name will appear in UI
            __('Ebooks Widget', 'wpbe_widget_domain'),

            // Widget description
            array('description' => __('Sample widget based on WPBeginner Tutorial', 'wpbe_widget_domain'),)
        );
    }

    // Creating widget front-end

    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);

        $category = apply_filters('widget_category', $instance['category']);

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];


        // This is where you run the code and display the output



        $args = array(
            'post_type'      => 'reports',
            'posts_per_page' => 30,
        );

        $loop = new WP_Query($args);
        
    ?>
        <section class="related products woocommerce">

            <ul class="products columns-3 row">

                <?php
                while ($loop->have_posts()) : $loop->the_post();
                
$file = get_field('url_landing_page');


                    $content = apply_filters('the_content', get_the_content());


                    echo __('
                    <style>

                    
                    .woocommerce ul.products li.product:hover{
                        box-shadow: initial !important;
                        opacity: initial !important;
    -webkit-transform: translate(0,-4px);
    -moz-transform: translate(0,-4px);
    transform: initial !important;
    -webkit-transition: opacity .2s ease-out,-webkit-box-shadow .3s ease-out,-webkit-transform .3s ease-out;
    transition: opacity .2s ease-out,-webkit-box-shadow .3s ease-out,-webkit-transform .3s ease-out;
    -moz-transition: box-shadow .3s ease-out,transform .3s ease-out,opacity .2s ease-out,-moz-box-shadow .3s ease-out,-moz-transform .3s ease-out;
    transition: box-shadow .3s ease-out,transform .3s ease-out,opacity .2s ease-out;
    transition: initial;
    -webkit-transition-delay: .1s;
    -moz-transition-delay: .1s;
    transition-delay: initial !important;
                    }
                    </style>

            <li class="grid-item product post-12881 type-product status-publish has-post-thumbnail product_cat-wilco first instock shipping-taxable purchasable product-type-simple">
           
             ' .  get_the_post_thumbnail($loop->ID, 'full') . '
             <h2 class="woocommerce-loop-product__title" style="
             color: #db9224!important;
             font-size: 40px;
             line-height: 60px;
             font-weight: 800;
             display: block;
             text-align: center;
             clear: both;
             margin: 0px 0px 0px 0px;
             padding: 10px 0px 0px 0px;
             border-radius: 0px 0px 0px 0px;
             background: #ffffff;
             position: relative;
             z-index: 2 !important;
             border-top-width: 0px;
             border-right-width: 0px;
             border-bottom-width: 0px;
             border-left-width: 0px;
             border-color: #4cd964;
             border-style: solid;
         ">' . get_the_title() . '</h2>
         <div class="esg-bottom eg-post-13395 eg-washingtone-075257-element-3 text-center">' . $content . '</div>
         <div class="esg-bottom eg-post-13408 eg-washingtone-075257-element-13-a" style="
         display: block !important;
         text-align: center !important;
         clear: none !important;
         margin: 0px 40px 30px 40px !important;
     ">
            <a href="' . $file . '" target="_blank" data-quantity="1" class="eg-washingtone-075257-element-13" data-product_id="12881" data-product_sku=" rel="nofollow" style="
            font-size: 14px !important;
            line-height: 21px !important;
            color: #ffffff !important;
            font-weight: 700 !important;
            padding: 12px 48px 12px 48px !important;
            border-radius: 100px 100px 100px 100px !important;
            background: #e5a300 !important;
            z-index: 2 !important;
            display: block;
            font-family: montserrat !important;
            text-transform: uppercase !important;
            letter-spacing: 2px !important;
        ">Download</a>
            </div>
            </li>
    
                
                
        
    
        ', 'wpbe_widget_domain');

                endwhile;
                ?>
            </ul>
        </section>


    <?php

        wp_reset_query();



        echo $args['after_widget'];
    }

    // Widget Backend 
    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpbe_widget_domain');
        }
        // Widget admin form
    ?>
        <p>
            <select name="category">
                <?php

                $categories = get_terms(['taxonomy' => 'product_cat']);

                // Getting a visual raw output
                foreach ($categories as $categoria) {
                    echo "<option value=" . $categoria->slug . ">" . $categoria->name . "</option>";
                }

                ?>
            </select>


            </select>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('teste:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
<?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
} // Class wpbe_widget ends here
