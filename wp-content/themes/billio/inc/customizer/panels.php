<?php
/**
 * This file config of panels customizer.
 *
 * @package Billio
 */

$billio_customizer_panels['site-identity-panel'] = array(
	'title' => esc_html__( 'Site Identities', 'billio' ),
	'description' => esc_html__( 'Control your blog setting\'s, layout, sidebar position', 'billio' ),
	'priority' => 10,
);

$billio_customizer_panels['footer-panel'] = array(
	'title' => esc_html__( 'Footer', 'billio' ),
	'description' => esc_html__( 'Control your footer setting\'s', 'billio' ),
	'priority' => 110,
);
