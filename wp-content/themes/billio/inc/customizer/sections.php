<?php
/**
 * This file config of sections for customizer.
 *
 * @package Billio
 */

$billio_customizer_sections['upload-logo'] = array(
	'title' => esc_html__( 'Logo', 'billio' ),
	'panel' => 'site-identity-panel',
	'priority' => 10,
);

$billio_customizer_sections['address-icon'] = array(
	'title' => esc_html__( 'Favicon', 'billio' ),
	'panel' => 'site-identity-panel',
	'priority' => 40,
);

$billio_customizer_sections['colors'] = array(
	'title' => esc_html__( 'Brand Color', 'billio' ),
	'panel' => 'site-identity-panel',
	'priority' => 60,
);

$billio_customizer_sections['copyright-text'] = array(
	'title' => esc_html__( 'Copyright Text', 'billio' ),
	'panel' => 'site-identity-panel',
	'priority' => 100,
);

// Billio sections.
$billio_customizer_sections['layout-option'] = array(
	'title' => esc_html__( 'Layout Options', 'billio' ),
	'panel' => '',
	'priority' => 20,
);

// Billio Navigation Bar Sticky Menu.
$billio_customizer_sections['menu-option'] = array(
	'title' => esc_html__( 'Navigation Bar', 'billio' ),
	'panel' => '',
	'priority' => 20,
);

// Billio Top Bar.
$billio_customizer_sections['top-bar'] = array(
	'title' => esc_html__( 'Top Bar', 'billio' ),
	'panel' => '',
	'priority' => 30,
);

// Move WordPress default sections to billio sections.
$wp_customizer_sections['static_front_page'] = array(
	'priority' => 40,
);

$wp_customizer_sections['title_tagline'] = array(
	'panel' => 'site-identity-panel',
	'title' => esc_html__( 'Site Title', 'billio' ),
	'priority' => 20,
);

$wp_customizer_sections['header_image'] = array(
	'title' => esc_html__( 'Header Image', 'billio' ),
	'priority' => 100,
);

if ( ! function_exists( 'detheme_display_footer_builder' ) ) {
	$billio_customizer_sections['footer-section'] = array(
		'title' => esc_html__( 'Footer', 'billio' ),
		'panel' => 'site-identity-panel',
		'priority' => 110,
	);
}

$wp_customizer_sections['custom_css'] = array(
	'title' => esc_html__( 'Custom CSS', 'billio' ),
	'priority' => 120,
);
