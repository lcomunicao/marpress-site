<?php
/**
 * This file config of custom control for customizer.
 *
 * @package Billio
 */

$billio_customizer_settings['primary-color'] = array(
	'section'     => 'colors',
	'type'        => 'color',
	'default'     => '#e5a300',
	'transport'   => 'postMessage',
	'label'       => esc_html__( 'Brand Color', 'billio' ),
	'description' => esc_html__( 'Change your brand color', 'billio' ),
);
