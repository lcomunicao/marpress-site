<?php
/**
 * This file config of custom control for customizer.
 *
 * @package Billio
 */

$billio_customizer_settings['logo-height'] = array(
	'section'         => 'upload-logo',
	'type'            => 'custom',
	'custom_type'     => 'billio-input-slider',
	'default'         => 27,
	'unit'            => 'px',
	'priority'        => 9,
	'label'           => esc_html__( 'Height', 'billio' ),
	'description'     => esc_html__( 'Recomended logo height is 21px.' , 'billio' ),
	'transport'       => 'postMessage',
	'input_attrs'     => array(
		'min'  => 10,
		'max'  => 250,
		'step' => 1,
	),
);

$billio_customizer_settings['logo-sticky'] = array(
	'section'         => 'upload-logo',
	'type'            => 'image',
	'priority'        => 9,
	'default'         => '',
	'label'           => esc_html__( 'Logo Sticky', 'billio' ),
	'transport'       => 'refresh',
);

$billio_customizer_settings['logo-sticky-height'] = array(
	'section'         => 'upload-logo',
	'type'            => 'custom',
	'custom_type'     => 'billio-input-slider',
	'default'         => 27,
	'unit'            => 'px',
	'priority'        => 9,
	'label'           => esc_html__( 'Logo Sticky Height', 'billio' ),
	'description'     => esc_html__( 'Recomended logo height is 21px.' , 'billio' ),
	'transport'       => 'postMessage',
	'input_attrs'     => array(
		'min'  => 10,
		'max'  => 250,
		'step' => 1,
	),
);

$billio_customizer_settings['blog-layout'] = array(
	'section'         => 'layout-option',
	'type'            => 'custom',
	'custom_type'     => 'billio-switcher',
	'default'         => 'billio-layout-fullwidth',
	'label'           => esc_html__( 'Blog Layout', 'billio' ),
	'transport'       => 'postMessage',
	'choices'         => array(
		'billio-layout-fullwidth'  => esc_html__( 'Fullwidth' , 'billio' ),
		'billio-layout-boxed'      => esc_html__( 'Boxed' , 'billio' ),
	),
);

$billio_customizer_settings['style-layout'] = array(
	'section'         => 'layout-option',
	'type'            => 'custom',
	'custom_type'     => 'billio-switcher',
	'default'         => 'billio-classic-layout',
	'label'           => esc_html__( 'Layout Styles', 'billio' ),
	'transport'       => 'refresh',
	'choices'         => array(
		'billio-classic-layout'  => esc_html__( 'Classic' , 'billio' ),
		'billio-masonry-layout'     => esc_html__( 'Masonry' , 'billio' ),
	),
);

$billio_customizer_settings['sidebar-layout'] = array(
	'section'         => 'layout-option',
	'type'            => 'custom',
	'custom_type'     => 'billio-image-selector',
	'default'         => 'sidebar-right',
	'transport'       => 'refresh',
	'label'           => esc_html__( 'Sidebar Option', 'billio' ),
	'description'     => esc_html__( 'You can choose sidebar position on your site, is it in right, left, or no sidebar.' , 'billio' ),
	'choices'         => array(
		'sidebar-right'  => array(
			'image' => BILLIO_THEME_URI . '/inc/customizer/assets/images/rs.png',
			'name' => esc_html__( 'Right Sidebar' , 'billio' ),
		),
		'sidebar-none'  => array(
			'image' => BILLIO_THEME_URI . '/inc/customizer/assets/images/ns.png',
			'name' => esc_html__( 'None Sidebar' , 'billio' ),
		),
		'sidebar-left'  => array(
			'image' => BILLIO_THEME_URI . '/inc/customizer/assets/images/ls.png',
			'name' => esc_html__( 'Left Sidebar' , 'billio' ),
		),
	),
);

$billio_customizer_settings['parallax-layout'] = array(
	'section'         => 'layout-option',
	'type'            => 'custom',
	'custom_type'     => 'billio-switcher',
	'default'         => 'billio-header-parallax',
	'label'           => esc_html__( 'Header Image Scrolling', 'billio' ),
	'transport'       => 'refresh',
	'choices'         => array(
		'billio-header-parallax' => esc_html__( 'Parallax' , 'billio' ),
		'billio-header-none' => esc_html__( 'Static' , 'billio' ),
	),
);

$billio_customizer_settings['dropdown-layout'] = array(
	'section'         => 'layout-option',
	'type'            => 'custom',
	'custom_type'     => 'billio-switcher',
	'default'         => 'billio-dropdown-style',
	'label'           => esc_html__( 'Dropdown Style', 'billio' ),
	'transport'       => 'refresh',
	'choices'         => array(
		'billio-dropdown-style' => esc_html__( 'Selectize' , 'billio' ),
		'billio-dropdown-none' => esc_html__( 'Original' , 'billio' ),
	),
);

$billio_customizer_settings['related-post-image'] = array(
	'section'     => 'layout-option',
	'type'        => 'custom',
	'custom_type' => 'billio-switcher',
	'label'       => esc_html__( 'Related Post Image', 'billio' ),
	'default'     => 'related-post-image-hidden',
	'choices'     => array(
		'related-post-image-hidden'  => esc_html__( 'Hidden' , 'billio' ),
		'related-post-image-show'    => esc_html__( 'Show' , 'billio' ),
	),
);

$billio_customizer_settings['breadcrumbs-option'] = array(
	'section'     => 'layout-option',
	'type'        => 'custom',
	'custom_type' => 'billio-switcher',
	'label'       => esc_html__( 'Breadcrumbs', 'billio' ),
	'default'     => 'breadcrumbs-show',
	'choices'     => array(
		'breadcrumbs-hidden'  => esc_html__( 'Hidden' , 'billio' ),
		'breadcrumbs-show'    => esc_html__( 'Show' , 'billio' ),
	),
);

$billio_customizer_settings['sticky-layout'] = array(
	'section'         => 'menu-option',
	'type'            => 'custom',
	'custom_type'     => 'billio-switcher',
	'default'         => 'billio-sticky',
	'label'           => esc_html__( 'Navigation Position', 'billio' ),
	'transport'       => 'refresh',
	'choices'         => array(
		'billio-sticky' => esc_html__( 'Sticky' , 'billio' ),
		'billio-scroll' => esc_html__( 'Scroll' , 'billio' ),
	),
);

$billio_customizer_settings['navbar-font-color'] = array(
	'section'     => 'menu-option',
	'type'        => 'color',
	'default'     => '#333333',
	'transport'   => 'refresh',
	'label'       => esc_html__( 'Font & Background Color', 'billio' ),
);

$billio_customizer_settings['navbar-background-color'] = array(
	'section'     => 'menu-option',
	'type'        => 'color-alpha',
	'default'     => '#ffffff',
	'capability'  => 'edit_theme_options',
	'transport'   => 'refresh',
	'show_opacity'  => true,
	'palette'   => array(
		'rgb(150, 50, 220)',
		'rgba(50,50,50,0.8)',
		'rgba( 255, 255, 255, 0.2 )',
		'#00CC99',
	),
);

$billio_customizer_settings['navbar-font-sticky-color'] = array(
	'section'     => 'menu-option',
	'type'        => 'color',
	'default'     => '#333333',
	'transport'   => 'refresh',
	'label'       => esc_html__( 'Font & Background Color Sticky', 'billio' ),
);

$billio_customizer_settings['navbar-background-sticky-color'] = array(
	'section'     => 'menu-option',
	'type'        => 'color-alpha',
	'default'     => '#ffffff',
	'capability'  => 'edit_theme_options',
	'transport'   => 'refresh',
	'show_opacity'  => true,
	'palette'   => array(
		'rgb(150, 50, 220)',
		'rgba(50,50,50,0.8)',
		'rgba( 255, 255, 255, 0.2 )',
		'#00CC99',
	),
);

$billio_customizer_settings['navbar-bottom-border'] = array(
	'section'         => 'menu-option',
	'type'            => 'custom',
	'custom_type'     => 'billio-switcher',
	'default'         => '1px solid #e3e3e3',
	'label'           => esc_html__( 'Bottom Border on Homepage', 'billio' ),
	'transport'       => 'refresh',
	'choices'         => array(
		'none'              => esc_html__( 'Hide' , 'billio' ),
		'1px solid #e3e3e3' => esc_html__( 'Show' , 'billio' ),
	),
);
