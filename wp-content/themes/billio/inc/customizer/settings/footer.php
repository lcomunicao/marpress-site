<?php
/**
 * This file config of custom control for customizer.
 *
 * @package Billio
 */

if ( function_exists( 'detheme_display_footer_builder' ) ) {
	$billio_customizer_settings['footer-option'] = array(
		'section'         => 'footer-section',
		'type'            => 'select',
		'default'         => 'footer-widget',
		'label'           => esc_html__( 'Select Footer', 'billio' ),
		'transport'       => 'refresh',
		'description'     => esc_html__( 'Select widget or page to appear on Footer.' , 'billio' ),
		'choices'         => array(
			'footer-widget' => esc_html__( 'Footer from Widget' , 'billio' ),
			'footer-page'   => esc_html__( 'Footer from Page' , 'billio' ),
		),
	);

	$billio_customizer_settings['footer-url'] = array(
		'section'         => 'footer-section',
		'type'            => 'custom',
		'custom_type'     => 'billio-link',
		'default'         => '',
		'label'           => esc_html__( 'Click here to edit footer widgets', 'billio' ),
		'description'     => esc_html__( '#', 'billio' ),
		'transport'       => 'refresh',
	);

	$billio_customizer_settings['footer-content'] = array(
		'section'         => 'footer-section',
		'type'            => 'select',
		'default'         => 'billio-page-1',
		'label'           => esc_html__( 'Footer from Page', 'billio' ),
		'transport'       => 'refresh',
		'description'     => esc_html__( 'Select a page to appear on Footer.' , 'billio' ),
		'choices'         => detheme_get_pages_array(),
	);
}

$billio_customizer_settings['footer-display-copyright'] = array(
	'section'     => 'footer-section',
	'type'        => 'custom',
	'custom_type' => 'billio-switcher',
	'label'       => esc_html__( 'Copyright Text', 'billio' ),
	'description' => esc_html__( 'Write your copyright', 'billio' ),
	'default'     => true,
	'choices'     => array(
		false   => esc_html__( 'Hidden' , 'billio' ),
		true    => esc_html__( 'Show' , 'billio' ),
	),
);

/* translators: %s: site legal */
$default = sprintf( esc_html__( 'Copyright %s', 'billio' ), get_bloginfo( 'name' ) );

$billio_customizer_settings['footer-legal'] = array(
	'section'     => 'footer-section',
	'type'        => 'textarea',
	'default'     => $default,
	'transport'   => 'postMessage',
	'input_attrs' => array(
		'class' => 'my-custom-class',
		'placeholder' => esc_html__( 'Enter message...', 'billio' ),
	),
);
