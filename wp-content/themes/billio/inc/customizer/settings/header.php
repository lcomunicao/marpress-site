<?php
/**
 * This file config of custom control for customizer.
 *
 * @package Billio
 */

$billio_customizer_settings['header-styling'] = array(
	'section'     => 'header-styling',
	'type'        => 'radio',
	'default'     => 'layout-boxed',
	'label'       => esc_html__( 'Header Style', 'billio' ),
	'description' => esc_html__( 'This option affects your header layout. ( This control just temporary )' , 'billio' ),
	'choices'     => array(
		'style-1' => esc_html__( 'Style 1', 'billio' ),
		'style-2' => esc_html__( 'Style 2', 'billio' ),
		'style-3' => esc_html__( 'Style 3', 'billio' ),
		'style-4' => esc_html__( 'Style 4', 'billio' ),
	),
);

$billio_customizer_settings['header_image_show_frontpage'] = array(
	'section'     => 'header_image',
	'type'        => 'custom',
	'custom_type' => 'billio-switcher',
	'default'     => 'billio-header-image-show',
	'label'       => esc_html__( 'Display at Homepage', 'billio' ),
	'choices'         => array(
		'billio-header-image-show' => esc_html__( 'Show' , 'billio' ),
		'billio-header-image-hide' => esc_html__( 'Hide' , 'billio' ),
	),
);
