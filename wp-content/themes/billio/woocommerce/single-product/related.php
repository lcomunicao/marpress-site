<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



 if( have_rows('modelos') ): ?>

<section class="kc-elm kc-css-902021 kc_row"><div class="kc-row-container  kc-container"><div class="kc-wrap-columns"><div class="kc-elm kc-css-816257 kc_col-sm-12 kc_column kc_col-sm-12"><div class="kc-col-container">
<div class="kc-elm kc-css-134730 kc-title-wrap ">

	<h3 class="kc_title">Modelos e Especificações</h3>
</div>
</div></div></div></div></section>
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">Modelo</th>
          <th scope="col">Descrição</th>
          <th scope="col">Potência</th>
          <th scope="col">Dimensão</th>
          <th scope="col">Tensão</th>
          <th scope="col">Bulbo</th>
          <th scope="col">Potência Luminosa</th>
          <th scope="col">Carcaça</th>
          <th scope="col">Lente</th>
          <th scope="col">Cor da Luz</th>
          <th scope="col">IP</th>
    
        </tr>
      </thead>
      <tbody>
    
    
        <?php while( have_rows('modelos') ): the_row(); 
    
    
    $modelo = get_sub_field('modelo');
    $descricao = get_sub_field('descricao');
    $potencia = get_sub_field('potencia');
    $dimensao = get_sub_field('dimensao');
    $tensao = get_sub_field('tensao');
    $buibo = get_sub_field('buibo');
    $potencia_luminosa = get_sub_field('potencia_luminosa');
    $carcaca = get_sub_field('carcaca');
    $lente = get_sub_field('lente');
    $cor_da_luz = get_sub_field('cor_da_luz');
    $ip = get_sub_field('ip');
    
    
    
    
    ?>
    
    
    <tr>
          <th><?php echo $modelo; ?></th>
          <th><?php echo $descricao; ?></th>
          <th><?php echo $potencia; ?></th>
          <th><?php echo $dimensao; ?></th>
          <th><?php echo $tensao; ?></th>
          <th><?php echo $buibo; ?></th>
          <th><?php echo $potencia_luminosa; ?></th>
          <th><?php echo $carcaca; ?></th>
          <th><?php echo $lente ?></th>
          <th><?php echo $cor_da_luz ?></th>
          <th><?php echo $ip ?></th>
    
    
          
        </tr>
    
    
    <?php endwhile; ?>
    
    
      </tbody>
    </table>
    <?php endif; 

if ( true ) : 


  global $post;
  $category;

  $product_cat_id = array();

  $terms = get_the_terms( $post->ID, 'product_cat' );

  


  foreach ($terms as $term) {

      $product_cat_id[] = $term->term_id;
      $product_cat_name = $term->name;


  }?>



	<section class="related productsss">

	

    <h3 class="kc_title">Outros produtos <?php echo $product_cat_name; ?></h3>

    <?php 

    $args = array(
            'post_type'      => 'product',
            'posts_per_page' => -1,
            'tax_query'   => array(
              array(
                  'taxonomy'      => 'product_cat',
                  'field' => 'term_id', //This is optional, as it defaults to 'term_id'
                  'terms'         => $product_cat_id,
                  'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
              )
          )
        );

        $loop = new WP_Query($args);
        ?>
        <section class="related products woocommerce">
            
        <ul class="products columns-4 row">

        <?php
        while ($loop->have_posts()) : $loop->the_post();
            global $product;
    

            echo __('
                
            <li class="grid-item product post-12881 type-product status-publish has-post-thumbnail product_cat-wilco first instock shipping-taxable purchasable product-type-simple">
            <a href="'. get_permalink() .'" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
             '. woocommerce_get_product_thumbnail() . '
                <h2 class="woocommerce-loop-product__title">'. get_the_title() .'</h2>
            </a>
            <a href="'. get_permalink() .'" data-quantity="1" class="button product_type_simple" data-product_id="12881" data-product_sku="" aria-label="Add “JCB Overhaul Kit Phosfluorescent STD” to your cart" rel="nofollow">Ver produto</a>
            </li>
    
                
                
        
    
        ', 'wpb_widget_domain');

        endwhile;
        ?>
        </ul>
        </section>

	</section>

<?php endif;

wp_reset_postdata();

