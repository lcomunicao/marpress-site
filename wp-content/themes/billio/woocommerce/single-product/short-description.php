<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $post;

$short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );

if ( ! $short_description ) {
	return;
}

?>
<div class="woocommerce-product-details__short-description">
	<?php echo $short_description; // WPCS: XSS ok. ?>
</div>



<!-- /FIM EXIBIR ICONES DINAMICOS  -->
						<!-- <?php 

						$url = get_field('url');

						if( have_rows('icones') ): ?>

							<h5 style="
						    font-weight: 900;
						">Uso indicado para</h5>

						<div class="row">

						<?php while( have_rows('icones') ): the_row(); 


						$image = get_sub_field('icone');
						$content = get_sub_field('texto');
						?>

						<div class="col-2">
						<div style="
						    min-height: 2rem;
						">
							<img src="<?php echo $image['url'];?>" alt="Minha Figura" style="
						    width: 58px;
							height: 40px;
							text-align:center;
						">	</div>
									
								<div><p style="
						    font-weight: 900;
						    color: #000;
							font-size: 13px;
							text-align: center;
						    padding: 0.8rem 0rem;
						">	<?php echo  $content; ?></p></div>
								</div>

						<?php endwhile; ?>

						</div>
						<?php endif; ?> -->
	<!-- /FIM EXIBIR ICONES DINAMICOS  -->

<!-- EXIBIR ICONES ESTATICOS -->
<h5 style=" font-weight: 900;">Uso indicado para</h5>

<div class="row">

<!-- 	<div class="col-3">
		<div style="min-height: 2rem; text-align:center; ">
			<img src="/wp-content/uploads/2015/04/Automotivo-e1584734375509.png" alt="Minha Figura" style="
		    width: 58px;
			height: 40px;
			text-align:center;">	
		</div>
				
		<div><p style="
		    font-weight: 900;
		    color: #000;
			font-size: 13px;
			text-align: center;
		    padding: 0.8rem 0rem 0rem 0rem;
			">	Automotivo</p>
		</div>
	</div> -->


	<div class="col-3">
		<div style="min-height: 2rem; text-align:center; ">
			<img src="/wp-content/uploads/2015/04/industria--e1584734438201.png" alt="Minha Figura" style="
		    width: 58px;
			height: 40px;
			text-align:center;">	
		</div>
				
		<div><p style="
		    font-weight: 900;
		    color: #000;
			font-size: 13px;
			text-align: center;
		    padding: 0.8rem 0rem 0rem 0rem;
			">	Industrial</p>
		</div>
	</div>


	<div class="col-3">
		<div style="min-height: 2rem; text-align:center; ">
			<img src="/wp-content/uploads/2015/04/metalurgia-e1584734498368.png" alt="Minha Figura" style="
		    width: 58px;
			height: 40px;
			text-align:center;">	
		</div>
				
		<div><p style="
		    font-weight: 900;
		    color: #000;
			font-size: 13px;
			text-align: center;
		    padding: 0.8rem 0rem 0rem 0rem;
			">	Metalurgia</p>
		</div>
	</div>


	<div class="col-3">
		<div style="min-height: 2rem; text-align:center; ">
			<img src="/wp-content/uploads/2015/04/oleo-e-gas-e1584734536286.png" alt="Minha Figura" style="
		    width: 58px;
			height: 40px;
			text-align:center;">	
		</div>
					
		<div><p style="
		    font-weight: 900;
		    color: #000;
			font-size: 13px;
			text-align: center;
		    padding: 0.8rem 0rem 0rem 0rem;
			">	Óleo e Gás</p>
		</div>
	</div>


	<div class="col-3">
		<div style="min-height: 2rem; text-align:center; ">
			<img src="/wp-content/uploads/2015/04/reflorestamento-e1584734592671.png" alt="Minha Figura" style="
		    width: 58px;
			height: 40px;
			text-align:center;">	
		</div>
					
		<div><p style="
		    font-weight: 900;
		    color: #000;
			font-size: 13px;
			text-align: center;
		    padding: 0.8rem 0rem 0rem 0rem;
			">	Reflorestamento</p>
		</div>
	</div>


	<div class="col-3">
		<div style="min-height: 2rem; text-align:center; ">
			<img src="/wp-content/uploads/2015/04/termoeletrica-e1585706529618.png" alt="Minha Figura" style="
		    width: 58px;
			height: 40px;
			text-align:center;">	
		</div>
				
		<div><p style="
		    font-weight: 900;
		    color: #000;
			font-size: 13px;
			text-align: center;
		    padding: 0.8rem 0rem 0rem 0rem;
			">	Termoelétricas</p>
		</div>
	</div>


	<div class="col-3">
		<div style="min-height: 2rem; text-align:center; ">
			<img src="/wp-content/uploads/2015/04/caminhao2-1-5-1.png" alt="Minha Figura" style="
		    width: 58px;
			height: 40px;
			text-align:center;">	
		</div>
				
		<div><p style="
		    font-weight: 900;
		    color: #000;
			font-size: 13px;
			text-align: center;
		    padding: 0.8rem 0rem 0rem 0rem;
			">	Mineração</p>
		</div>
	</div>


	<div class="col-3">
		<div style="min-height: 2rem; text-align:center; ">
			<img src="/wp-content/uploads/2015/04/gundast-2.png" alt="Minha Figura" style="
		    width: 58px;
			height: 40px;
			text-align:center;">	
		</div>
				
		<div><p style="
		    font-weight: 900;
		    color: #000;
			font-size: 13px;
			text-align: center;
		    padding: 0.8rem 0rem 0rem 0rem;
			">	Construção</p>
		</div>
	</div>


	<div class="col-3">
		<div style="min-height: 2rem; text-align:center; ">
			<img src="/wp-content/uploads/2015/04/trator-3-1-1.png" alt="Minha Figura" style="
		    width: 58px;
			height: 40px;
			text-align:center;">	
		</div>
				
		<div><p style="
		    font-weight: 900;
		    color: #000;
			font-size: 13px;
			text-align: center;
		    padding: 0.8rem 0rem 0rem 0rem;
			">	Agricultura</p>
		</div>
	</div>
</div>
<!-- /FIM EXIBIR ICONES ESTATICOS -->



<form class="cart" id="cart">
		
<?php if($url){?>
	<a href="<?php echo $url; ?>"  target="_blank" type="submit" name="add-to-cart" value="12895" class="single_add_to_cart_button button alt">Baixe a ficha técnica do produto</a>
<?php } ?>

</form>

