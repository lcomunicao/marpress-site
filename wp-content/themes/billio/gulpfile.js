const path = require('path')
const gulp = require('gulp')
const sass = require('gulp-sass')
const rename = require('gulp-rename')
const merge = require('merge2')
const clone = require('gulp-clone')
const cleanCSS = require('gulp-clean-css')
const sourceMaps = require('gulp-sourcemaps')
const rollup = require('rollup-stream')
const source = require('vinyl-source-stream')
const buffer = require('vinyl-buffer')
const babel = require('rollup-plugin-babel')
const rtlcss = require('gulp-rtlcss')
const autoPrefixr = require('gulp-autoprefixer')
const uglify = require('gulp-uglify')
const plumber = require('gulp-plumber')
const wpPot = require('gulp-wp-pot')
const gettext = require('gulp-gettext')
const cssbeautify = require('gulp-cssbeautify')
const replacer = require('gulp-replace')
const stripComments = require('gulp-strip-css-comments')
const cleanup = require('rollup-plugin-cleanup')

const pkg = require('./package.json')

// Source Path
const srcPath = {
  ASSET_SASS: path.join(__dirname, 'assets-src', 'sass'),
  ASSET_JS: path.join(__dirname, 'assets-src', 'js'),
  CUSTOMIZER_ASSET_SASS: path.join(__dirname, 'inc', 'customizer', 'assets-src', 'sass'),
  CUSTOMIZER_ASSET_JS: path.join(__dirname, 'inc', 'customizer', 'assets-src', 'js')
}

// Destination Path
const dstPath = {
  ASSET_CSS: path.join(__dirname, 'assets', 'css'),
  ASSET_JS: path.join(__dirname, 'assets', 'js'),
  CUSTOMIZER_ASSET_CSS: path.join(__dirname, 'inc', 'customizer', 'assets', 'css'),
  CUSTOMIZER_ASSET_JS: path.join(__dirname, 'inc', 'customizer', 'assets', 'js')
}

// Automatically standarizing coding style across files
const sassCompiler = (inputFile, options) => {
  return () => {
    // Compile Default Style
    const defaultStyle = gulp.src(path.join(options.srcPath, inputFile))
    .pipe(plumber())
    .pipe(replacer(/\/\*rtl(.[^/]*)\*\//g, '/*!rtl$1*/'))
    .pipe(replacer(/\/\*!rtl(.[^/]*)\*\/;/g, ';/*!rtl:after$1*/'))
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(autoPrefixr({
      browsers: ['last 2 versions', 'Firefox < 20'],
      cascade: true
    }))

    // Compile RTL only when options.rtl is given
    let RTLStyle
    let RTLMinStyle

    if (options.rtl === true) {
      // Compile RTL Style
      RTLStyle = defaultStyle.pipe(clone())
        .pipe(replacer(/\/\*!rtl(.[^/]*)\*\//g, '/*rtl$1*/'))
        .pipe(replacer(/;\/\*rtl:after(.[^/]*)\*\//g, '/*rtl$1*/;'))
        .pipe(rtlcss())
        .pipe(cssbeautify({indent: '  ', autosemicolon: true}))
        .pipe(replacer(/}\/\*!/g, '}\n\n/*!'))
        .pipe(replacer(/\*\//g, '*/\n'))
        .pipe(rename({suffix: '-rtl'}))
        .pipe(gulp.dest(options.dstPath))

      // Compile RTL Minified Style
      RTLMinStyle = RTLStyle.pipe(clone())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(stripComments({preserve: false}))
        .pipe(sourceMaps.init())
        .pipe(rename({basename: 'theme', suffix: '.min-rtl'}))
        .pipe(sourceMaps.write('./'))
        .pipe(gulp.dest(options.dstPath))
    }

    const style = defaultStyle
      .pipe(replacer(/\/\*rtl(.[^/]*)\*\//g, ''))
      .pipe(cssbeautify({indent: '  ', autosemicolon: true}))
      .pipe(replacer(/}\/\*!/g, '}\n\n/*!'))
      .pipe(replacer(/\*\//g, '*/\n'))
      .pipe(gulp.dest(options.dstPath))

    // Compile Minified Style
    const minStyle = style.pipe(clone())
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(sourceMaps.init())
      .pipe(rename({suffix: '.min'}))
      .pipe(stripComments({preserve: false}))
      .pipe(sourceMaps.write('./'))
      .pipe(gulp.dest(options.dstPath))

    return merge([style, minStyle], options.rtl === true ? [RTLStyle, RTLMinStyle] : [])
  }
}

// Javascript Bundler
const jsCompiler = (inputFile, options) => {
  return () => rollup({
    input: path.join(options.srcPath, inputFile),
    sourcemap: false,
    format: 'iife',
    plugins: [
      babel({
        presets: [
          ['env', {modules: false}]
        ],
        babelrc: false
      }),
      cleanup({
        comments: ['jsdoc']
      })
    ]
  })
    // Beautify
    .pipe(source(inputFile, options.srcPath))
    .pipe(plumber())
    .pipe(buffer())
    .pipe(gulp.dest(options.dstPath))
    // Minify
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(uglify())
    .pipe(sourceMaps.init())
    .pipe(sourceMaps.write('./'))
    .pipe(gulp.dest(options.dstPath))
}

gulp.task('compile:bootstrap', sassCompiler('bootstrap.scss', {
  srcPath: srcPath.ASSET_SASS,
  dstPath: dstPath.ASSET_CSS
}))

gulp.task('compile:theme-style', sassCompiler('theme.scss', {
  srcPath: srcPath.ASSET_SASS,
  dstPath: dstPath.ASSET_CSS,
  rtl: true
}))

gulp.task('compile:theme-js', jsCompiler('theme.js', {
  srcPath: srcPath.ASSET_JS,
  dstPath: dstPath.ASSET_JS
}))

gulp.task('compile:bootstrap-js', () => {
  const bootstrapPath = path.dirname(require.resolve('bootstrap'))
  const compileBootstrap = gulp
    .src(path.join(bootstrapPath, 'bootstrap.js'))
    .pipe(gulp.dest(dstPath.ASSET_JS))

  const compileBootstrapMin = gulp
    .src(path.join(bootstrapPath, 'bootstrap.min.js'))
    .pipe(gulp.dest(dstPath.ASSET_JS))

  return merge(compileBootstrap, compileBootstrapMin)
})

gulp.task('compile:customizer-style', sassCompiler('customizer.scss', {
  srcPath: srcPath.CUSTOMIZER_ASSET_SASS,
  dstPath: dstPath.CUSTOMIZER_ASSET_CSS
}))

gulp.task('compile:customizer-preview', jsCompiler('customizer-preview.js', {
  srcPath: srcPath.CUSTOMIZER_ASSET_JS,
  dstPath: dstPath.CUSTOMIZER_ASSET_JS
}))

gulp.task('compile:customizer-panel', jsCompiler('customizer-panel.js', {
  srcPath: srcPath.CUSTOMIZER_ASSET_JS,
  dstPath: dstPath.CUSTOMIZER_ASSET_JS
}))

gulp.task('compile:languages', () => {
  const pot = gulp.src('./**/*.php')
    .pipe(wpPot({
      domain: pkg.theme.textDomain,
      package: pkg.theme.name
    }))
    .pipe(gulp.dest('./languages/billio.pot'))

  const mo = gulp.src('./languages/*.po')
    .pipe(gettext())
    .pipe(gulp.dest('./languages/'))

  return merge(pot, mo)
})

gulp.task('watch:files', () => {
  gulp.watch([
    path.join(srcPath.ASSET_SASS, '**', '*.scss'),
    path.join(srcPath.ASSET_SASS, 'theme.scss')
  ], ['compile:theme-style'])

  gulp.watch([
    path.join(srcPath.ASSET_SASS, 'bootstrap.scss')
  ], ['compile:bootstrap'])

  gulp.watch([path.join(srcPath.ASSET_JS, '**', '*.js')], [
    'compile:theme-js',
    'compile:bootstrap-js'
  ])

  gulp.watch([
    path.join(srcPath.CUSTOMIZER_ASSET_SASS, '**', '*.scss'),
    path.join(srcPath.CUSTOMIZER_ASSET_SASS, 'customizer.scss')
  ], ['compile:customizer-style'])

  gulp.watch([
    path.join(srcPath.CUSTOMIZER_ASSET_JS, '**', '*.js')
  ], ['compile:customizer-preview', 'compile:customizer-panel'])

  gulp.watch([path.join(__dirname, '**', '*.php')], [
    'compile:languages'
  ])
})

gulp.task('build', [
  'compile:bootstrap',
  'compile:theme-style',
  'compile:theme-js',
  'compile:customizer-style',
  'compile:customizer-preview',
  'compile:customizer-panel',
  'compile:languages',
  'compile:bootstrap-js'
])

gulp.task('default', ['watch:files'])
