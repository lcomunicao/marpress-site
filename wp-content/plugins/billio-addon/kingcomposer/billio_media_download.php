<?php

$classes = apply_filters( 'kc-el-class', $atts );
// This is for adding master class, so the css system can be worked with this element
$label   = $atts['media_download'];
$output  = '<div class="billio_media_download">';
$output .= '<select onChange="onChange(this)" class="billio_media  ' . implode( ' ', $classes ) . '">';
$output .= '<option selected value="#">' . strtoupper( $label ) . '</option>';
$output .= do_shortcode( str_replace( 'billio_media_download#', 'billio_media_download', $content ) );
// This to process its self nested
$output .= '</select>';
$output .= '</div>';

echo $output;

?>
<script>
window.onChange = function(select){
  // window.location.href= select.value;
  window.open( select.value,'_blank' );
}
</script>
