<?php

$classes = apply_filters( 'kc-el-class', $atts );
// This is for adding master class, so the css system can be worked with this element
$output  = '<div class="billio_media ' . implode( ' ', $classes ) . '">';
$output  = $atts['media_download'];
$output .= do_shortcode( str_replace( 'the_nested_element#', 'the_nested_element', $content ) );
// This to process its self nested
$output .= '</div>';

echo $output;


