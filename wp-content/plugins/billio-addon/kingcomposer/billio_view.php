<?php
/**
 * Document Viewer Template.
 *
 * @package billio Plugins
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
// print_r ($atts);
$download = $atts['download'];
$height   = $atts['height'];

?>
 
<div class='viewer'>
	<iframe src="https://docs.google.com/viewer?url=<?php echo wp_get_attachment_url( $atts['file'] ); ?>&embedded=true" frameborder="0" height="<?php echo $height; ?>px" width="100%"></iframe>

	<?php
	if ( $atts['download'] === 'yes' ) {
?>
		<a class="download" href="<?php echo wp_get_attachment_url( $atts['file'] ); ?>" ><?php _e( 'DOWNLOAD', 'billio' ); ?></a>
<?php
	}
?>
	</div>
