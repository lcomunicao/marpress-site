<?php
/**
 * Document Viewer Template.
 *
 * @package billio Plugins
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
// echo "Media Downloadte:<br />";
// print_r ($atts);
echo "<option id='billio_media_option' value='" . wp_get_attachment_url( $atts['file'] ) . "'>" . $atts['title'] . '</option>';
// $download = $atts['download'];
// $height   = $atts['height'];

