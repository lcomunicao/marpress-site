<?php
/**
 * Single Recruitments Template.
 *
 * @package billio Plugins
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$imageurl 			= "";
$alt_image 			= "";
$document_url       = "";
$document_extension = "";
$document_icon      = "";
$pre_title 			= ""; 
$button_label 		= "";


/* Get Image from featured image */
$thumb_id = get_post_thumbnail_id($post->ID);
$featured_image = wp_get_attachment_image_src($thumb_id,'full',false); 
if (isset($featured_image[0])) {
	$imageurl = $featured_image[0];
}

$alt_image = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);

$post_id = $post->ID;

$document_url       = get_post_meta($post_id,'_document_url',true);
$document_extension = get_post_meta($post_id,'_document_extension',true);
$document_icon      = get_post_meta($post_id,'_document_icon',true);
$pre_title 			= get_post_meta($post_id,'_pre_title',true); 
$button_label 		= get_post_meta($post_id,'_button_label', true );

$colsm = '';

get_header();
?>

<div class="container report">
<div id="report-<?php the_ID(); ?>" <?php post_class('col-xs-12 col-sm-6 equal_height_item'); ?>>
	<div class="row billio_item"> 
		<?php if ($imageurl!="") { 
			$colsm = 'col-sm-6';?>
			<div class="col-xs-12 col-sm-6">
				<img class="img-responsive" alt="<?php echo esc_attr($alt_image); ?>" src="<?php echo $imageurl; ?>">
			</div>
		<?php }?>
		
		<div class="col-xs-12 <?php echo sanitize_html_class($colsm); ?>">
			<h3 class="billio_pre_title"><?php echo $pre_title; ?></h3>
			<h2 class="billio_title"><?php the_title(); ?></h2>
			<div class="billio_content">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
			<?php if (!empty($document_url)) { ?>
				<div class="billio_button">
					<a href="<?php echo esc_url($document_url); ?>" target="_blank"><i class="<?php echo sanitize_html_class($document_icon); ?>"></i><?php echo $button_label . ' ' . $document_extension; ?></a>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
</div>

<?php
get_footer();
