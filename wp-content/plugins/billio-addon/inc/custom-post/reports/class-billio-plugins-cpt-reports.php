<?php
/**
 * Custom Posts Reports.
 *
 * @package billio Plugins
 */

defined( 'ABSPATH' ) || die();

/**
 * billio Plugins Custom Post Reports Class.
 *
 * Description.
 *
 * @since 1.0.0
 */
class billio_Plugins_Cpt_Reports {

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'register_billio_cpt_reports' ) );
		add_action( 'init', array( $this, 'register_billio_cpt_reports_categories' ) );
		add_filter( 'single_template', array( $this, 'cpt_reports_single_template' ) );
		add_theme_support( 'post-thumbnails' );
	}

	/**
	 * Register Custom Post Reports.
	 *
	 * @since 1.0.0
	 */
	public function register_billio_cpt_reports() {
		/**
		 * Post Type: Reports.
		 */
		$labels = array(
			'name'                  => __( 'E-books', 'billio-plugins' ),
			'singular_name'         => __( 'E-books', 'billio-plugins' ),
			'menu_name'             => __( 'E-books', 'billio-plugins' ),
			'all_items'             => __( 'All E-books', 'billio-plugins' ),
			'add_new'               => __( 'Add New', 'billio-plugins' ),
			'add_new_item'          => __( 'Add New E-books', 'billio-plugins' ),
			'edit_item'             => __( 'Edit E-books', 'billio-plugins' ),
			'new_item'              => __( 'New E-books', 'billio-plugins' ),
			'view_item'             => __( 'View E-books', 'billio-plugins' ),
			'view_items'            => __( 'View E-books', 'billio-plugins' ),
			'search_items'          => __( 'Search E-books', 'billio-plugins' ),
			'not_found'             => __( 'No E-books found', 'billio-plugins' ),
			'not_found_in_trash'    => __( 'No E-books found in trash', 'billio-plugins' ),
			'featured_image'        => __( 'Featured Image for E-books', 'billio-plugins' ),
			'set_featured_image'    => __( 'Set featured Image for E-books', 'billio-plugins' ),
			'remove_featured_image' => __( 'Remove featured image for E-books', 'billio-plugins' ),
			'use_featured_image'    => __( 'Use as featured Image for this E-books', 'billio-plugins' ),
		);

		$args = array(
			'label'                 => __( 'E-books', 'billio-plugins' ),
			'labels'                => $labels,
			'description'           => '',
			'public'                => true,
			'publicly_queryable'    => true,
			'show_ui'               => true,
			'delete_with_user'      => false,
			'show_in_rest'          => true,
			'rest_base'             => '',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'has_archive'           => false,
			'show_in_menu'          => true,
			'show_in_nav_menus'     => true,
			'exclude_from_search'   => false,
			'capability_type'       => 'post',
			'map_meta_cap'          => true,
			'hierarchical'          => true,
			'rewrite'               => array(
				'slug'       => 'reports',
				'with_front' => false,
			),
			'query_var'             => true,
			'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'author', 'page-attributes' ),
			'taxonomies'            => array( 'reports_category' ),
			'show_in_rest'          => true,
		);

		register_post_type( 'reports', $args );
		add_filter( 'manage_edit-reports_columns', array( $this, 'billio_show_reports_column' ) );
		add_action( 'manage_pages_custom_column', array( $this, 'billio_port_custom_columns' ) );
	}

	/**
	 * Custom Post List.
	 *
	 * @since 1.0.0
	 *
	 * @param string $columns   Column List.
	 */
	public function billio_show_reports_column( $columns ) {

		$columns = array(
			'cb'               => '<input type="checkbox" />',
			'title'            => __( 'Title', 'billio-plugins' ),
			'pre_title'        => __( 'Pre Title', 'billio-plugins' ),
			'document'         => __( 'Document', 'billio-plugins' ),
			'author'           => __( 'Author', 'billio-plugins' ),
			'reports-category' => __( 'Categories', 'billio-plugins' ),
			'date'             => __( 'Date', 'billio-plugins' ),
		);
		return $columns;
	}

	/**
	 * Add Image Column to Custom Post List.
	 *
	 * @since 1.0.0
	 *
	 * @param string $column   Image at First Column List.
	 */
	public function billio_port_custom_columns( $column ) {

		global $post;
		switch ( $column ) {
			case 'pre_title':
				echo get_post_meta( $post->ID, '_pre_title', true );
				break;
			case 'document':
				$document_url       = get_post_meta( $post->ID, '_document_url', true );
				$document_extension = get_post_meta( $post->ID, '_document_extension', true );
				$document_filename  = pathinfo( $document_url, PATHINFO_FILENAME );
				if ( ! empty( $document_url ) ) {
					echo '<a href="' . esc_url( $document_url ) . '" target="_blank">' . esc_html( $document_filename . '.' . $document_extension ) . '</a>';
				}
				break;
			case 'reports-category':
				echo get_the_term_list( $post->ID, 'reports_category', '', ', ', '' );
				break;
			case 'image':
				$attachment_id = get_the_post_thumbnail( $post->ID, 'thumbnail' );
				print ( $attachment_id ) ? $attachment_id : '';
				break;
		}
	}

	/**
	 * Register Custom Post Reports Categories.
	 *
	 * @since 1.0.0
	 */
	public function register_billio_cpt_reports_categories() {
		/**
		 * Taxonomy: Reports Categories.
		 */
		$labels = array(
			'name'          => __( 'Reports Categories', 'billio-plugins' ),
			'singular_name' => __( 'Reports Category', 'billio-plugins' ),
			'not_found'     => __( 'No category found', 'billio-plugins' ),
		);

		$args = array(
			'label'                 => __( 'Reports Categories', 'billio-plugins' ),
			'labels'                => $labels,
			'public'                => true,
			'publicly_queryable'    => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'show_in_nav_menus'     => true,
			'query_var'             => true,
			'rewrite'               => array(
				'slug'       => 'reports_category',
				'with_front' => true,
			),
			'show_admin_column'     => false,
			'show_in_rest'          => true,
			'rest_base'             => 'reports_category',
			'rest_controller_class' => 'WP_REST_Terms_Controller',
			'show_in_quick_edit'    => false,
		);
		register_taxonomy( 'reports_category', array( 'reports' ), $args );
	}

	/**
	 * Custom Post Reports Single Template.
	 *
	 * @since 1.0.0
	 *
	 * @param string $template   Template File URL.
	 */
	public function cpt_reports_single_template( $template ) {

		if ( is_singular( 'reports' ) ) {
			$template = billio_PLUGIN_DIR . 'inc/custom-post/reports/templates/single-reports.php';
		}

		return $template;
	}
}
