<?php
/**
 * Custom Posts Services.
 *
 * @package billio Plugins
 */

defined( 'ABSPATH' ) || die();

/**
 * billio Plugins Custom Post Services Class.
 *
 * Description.
 *
 * @since 1.0.0
 */
class billio_Plugins_Cpt_Services {

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'register_billio_cpt_service' ) );
		add_action( 'init', array( $this, 'register_billio_cpt_service_categories' ) );
		add_filter( 'single_template', array( $this, 'cpt_service_single_template' ) );
		add_theme_support( 'post-thumbnails' );
	}

	/**
	 * Register Custom Post Service.
	 *
	 * @since 1.0.0
	 */
	public function register_billio_cpt_service() {
		/**
		 * Post Type: Services.
		 */
		$labels = array(
			'name'                  => __( 'Services', 'billio-plugins' ),
			'singular_name'         => __( 'Service', 'billio-plugins' ),
			'menu_name'             => __( 'Services', 'billio-plugins' ),
			'all_items'             => __( 'All Services', 'billio-plugins' ),
			'add_new'               => __( 'Add New', 'billio-plugins' ),
			'add_new_item'          => __( 'Add New Service', 'billio-plugins' ),
			'edit_item'             => __( 'Edit Service', 'billio-plugins' ),
			'new_item'              => __( 'New Service', 'billio-plugins' ),
			'view_item'             => __( 'View Service', 'billio-plugins' ),
			'view_items'            => __( 'View Services', 'billio-plugins' ),
			'search_items'          => __( 'Search Service', 'billio-plugins' ),
			'not_found'             => __( 'No Services found', 'billio-plugins' ),
			'not_found_in_trash'    => __( 'No Services found in trash', 'billio-plugins' ),
			'featured_image'        => __( 'Featured Image for Service', 'billio-plugins' ),
			'set_featured_image'    => __( 'Set featured Image for Service', 'billio-plugins' ),
			'remove_featured_image' => __( 'Remove featured image for Service', 'billio-plugins' ),
			'use_featured_image'    => __( 'Use as featured Image for this Service', 'billio-plugins' ),
		);

		$args = array(
			'label'                 => __( 'Services', 'billio-plugins' ),
			'labels'                => $labels,
			'description'           => '',
			'public'                => true,
			'publicly_queryable'    => true,
			'show_ui'               => true,
			'delete_with_user'      => false,
			'show_in_rest'          => true,
			'rest_base'             => '',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'has_archive'           => false,
			'show_in_menu'          => true,
			'show_in_nav_menus'     => true,
			'exclude_from_search'   => false,
			'capability_type'       => 'post',
			'map_meta_cap'          => true,
			'hierarchical'          => true,
			'rewrite'               => array(
				'slug'       => 'service',
				'with_front' => false,
			),
			'query_var'             => true,
			'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'author', 'page-attributes' ),
			'taxonomies'            => array( 'service_category' ),
			'show_in_rest'          => true,
		);

		register_post_type( 'service', $args );
		add_filter( 'manage_edit-service_columns', array( $this, 'billio_show_service_column' ) );
		add_action( 'manage_pages_custom_column', array( $this, 'billio_port_custom_columns' ) );
	}

	/**
	 * Custom Post List.
	 *
	 * @since 1.0.0
	 *
	 * @param string $columns   Column List.
	 */
	public function billio_show_service_column( $columns ) {

		$columns = array(
			'cb'              => '<input type="checkbox" />',
			'image-services'  => __( 'Image', 'billio-plugins' ),
			'title'           => __( 'Title', 'billio-plugins' ),
			'author'          => __( 'Author', 'billio-plugins' ),
			'dtpost-category' => __( 'Categories', 'billio-plugins' ),
			'date'            => __( 'Date', 'billio-plugins' ),
		);
		return $columns;
	}

	/**
	 * Add Image Column to Custom Post List.
	 *
	 * @since 1.0.0
	 *
	 * @param string $column   Image at First Column List.
	 */
	public function billio_port_custom_columns( $column ) {

		global $post;
		switch ( $column ) {
			case 'service-category':
				echo get_the_term_list( $post->ID, 'service_category', '', ', ', '' );
				break;
			case 'image-services':
				$attachment_id = get_the_post_thumbnail( $post->ID, 'thumbnail' );
				print ( $attachment_id ) ? $attachment_id : '';
				break;
		}
	}

	/**
	 * Register Custom Post Service Categories.
	 *
	 * @since 1.0.0
	 */
	public function register_billio_cpt_service_categories() {
		/**
		 * Taxonomy: Service Categories.
		 */
		$labels = array(
			'name'          => __( 'Service Categories', 'billio-plugins' ),
			'singular_name' => __( 'Service Category', 'billio-plugins' ),
			'not_found'     => __( 'No category found', 'billio-plugins' ),
		);

		$args = array(
			'label'                 => __( 'Service Categories', 'billio-plugins' ),
			'labels'                => $labels,
			'public'                => true,
			'publicly_queryable'    => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'show_in_nav_menus'     => true,
			'query_var'             => true,
			'rewrite'               => array(
				'slug'       => 'service_category',
				'with_front' => true,
			),
			'show_admin_column'     => false,
			'show_in_rest'          => true,
			'rest_base'             => 'service_category',
			'rest_controller_class' => 'WP_REST_Terms_Controller',
			'show_in_quick_edit'    => false,
		);
		register_taxonomy( 'service_category', array( 'service' ), $args );
	}

	/**
	 * Custom Post Service Single Template.
	 *
	 * @since 1.0.0
	 *
	 * @param string $template   Template File URL.
	 */
	public function cpt_service_single_template( $template ) {

		if ( is_singular( 'service' ) ) {
			$template = billio_PLUGIN_DIR . 'inc/custom-post/service/templates/single-service.php';
		}

		return $template;
	}
}
