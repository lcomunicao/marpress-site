<?php
/**
 * Single Service Template.
 *
 * @package billio Plugins
 */

get_header();
?>

<div class="container">
	<div class="row">
		<article id="blog-post" class="billio-content order-lg-1 col-lg-12 col-md-12">
				<?php wp_reset_query(); ?>
				<?php
				while ( have_posts() ) :
					the_post();

				?>
				<div class="uf-single-post">

				<header class="uf-single-post__title" itemscope>

					<?php if ( ! has_header_image() ) : ?>
								<h1 class="uf-single-post-title entry-title" itemprop="headline"><?php the_title(); ?></h1><!-- .uf-single-post-title -->
							<?php endif; ?>
							
				</header>

				<div class="uf-single-post__content clearfix">
					<?php
						the_content();
					?>
					</div>
					</div>
				<?php endwhile; ?>
			</div>
		</article>
	</div>
</div>

<?php
get_footer();
