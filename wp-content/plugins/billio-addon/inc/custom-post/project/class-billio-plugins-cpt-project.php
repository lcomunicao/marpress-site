<?php
/**
 * Custom Posts Project.
 *
 * @package billio Plugins
 */

defined( 'ABSPATH' ) || die();

/**
 * billio Plugins Custom Post Project Class.
 *
 * Description.
 *
 * @since 1.0.0
 */
class billio_Plugins_Cpt_Project {

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'register_billio_cpt_project' ) );
		add_action( 'init', array( $this, 'register_billio_cpt_project_categories' ) );
		add_action( 'init', array( $this, 'register_billio_cpt_project_tags' ) );
		add_filter( 'single_template', array( $this, 'cpt_project_single_template' ) );
	}

	/**
	 * Register Custom Post Project.
	 *
	 * @since 1.0.0
	 */
	public function register_billio_cpt_project() {
		/**
		 * Post Type: Project.
		 */
		$labels = array(
			'name'                  => __( 'Categorias', 'billio-plugins' ),
			'singular_name'         => __( 'Categorias', 'billio-plugins' ),
			'menu_name'             => __( 'Categorias', 'billio-plugins' ),
			'all_items'             => __( 'All Categorias', 'billio-plugins' ),
			'add_new'               => __( 'Add New', 'billio-plugins' ),
			'add_new_item'          => __( 'Add New Categorias', 'billio-plugins' ),
			'edit_item'             => __( 'Edit Categorias', 'billio-plugins' ),
			'new_item'              => __( 'New Categorias', 'billio-plugins' ),
			'view_item'             => __( 'View Categorias', 'billio-plugins' ),
			'view_items'            => __( 'View Categorias', 'billio-plugins' ),
			'search_items'          => __( 'Search Categorias', 'billio-plugins' ),
			'not_found'             => __( 'No Categorias found', 'billio-plugins' ),
			'not_found_in_trash'    => __( 'No Categorias found in trash', 'billio-plugins' ),
			'featured_image'        => __( 'Featured Image for Categorias', 'billio-plugins' ),
			'set_featured_image'    => __( 'Set featured Image for Categorias', 'billio-plugins' ),
			'remove_featured_image' => __( 'Remove featured image for Categorias', 'billio-plugins' ),
			'use_featured_image'    => __( 'Use as featured Image for this Categorias', 'billio-plugins' ),
		);

		$args = array(
			'label'                 => __( 'Categorias', 'billio-plugins' ),
			'labels'                => $labels,
			'description'           => '',
			'public'                => true,
			'publicly_queryable'    => true,
			'show_ui'               => true,
			'delete_with_user'      => false,
			'show_in_rest'          => true,
			'rest_base'             => '',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'has_archive'           => false,
			'show_in_menu'          => true,
			'show_in_nav_menus'     => true,
			'exclude_from_search'   => false,
			'capability_type'       => 'post',
			'map_meta_cap'          => true,
			'hierarchical'          => true,
			'rewrite'               => array(
				'slug'       => 'project',
				'with_front' => false,
			),
			'query_var'             => true,
			'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'author', 'page-attributes' ),
			'taxonomies'            => array( 'Project_category', 'Project_tags' ),
			'show_in_rest'          => true,
		);

		register_post_type( 'project', $args );
	}

	/**
	 * Register Custom Post Project Categories.
	 *
	 * @since 1.0.0
	 */
	public function register_billio_cpt_project_categories() {
		/**
		 * Taxonomy: Project Categories.
		 */
		$labels = array(
			'name'          => __( 'Project Categories', 'billio-plugins' ),
			'singular_name' => __( 'Project Category', 'billio-plugins' ),
			'not_found'     => __( 'No category found', 'billio-plugins' ),
		);

		$args = array(
			'label'                 => __( 'Project Categories', 'billio-plugins' ),
			'labels'                => $labels,
			'public'                => true,
			'publicly_queryable'    => true,
			'hierarchical'          => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'show_in_nav_menus'     => true,
			'query_var'             => true,
			'rewrite'               => array(
				'slug'       => 'project_category',
				'with_front' => true,
			),
			'show_admin_column'     => false,
			'show_in_rest'          => true,
			'rest_base'             => 'project_category',
			'rest_controller_class' => 'WP_REST_Terms_Controller',
			'show_in_quick_edit'    => false,
		);
		register_taxonomy( 'project_category', array( 'project' ), $args );
	}

	/**
	 * Register billio Custom Post Project Tags.
	 *
	 * @since 1.0.0
	 */
	public function register_billio_cpt_project_tags() {
		/**
		 * Taxonomy: Project Tags.
		 */
		$labels = array(
			'name'          => __( 'Project Tags', 'billio-plugins' ),
			'singular_name' => __( 'Project Tag', 'billio-plugins' ),
		);

		$args = array(
			'label'                 => __( 'Project Tags', 'billio-plugins' ),
			'labels'                => $labels,
			'public'                => true,
			'publicly_queryable'    => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'show_in_nav_menus'     => true,
			'query_var'             => true,
			'rewrite'               => array(
				'slug'       => 'project_tags',
				'with_front' => true,
			),
			'show_admin_column'     => false,
			'show_in_rest'          => true,
			'rest_base'             => 'project_tags',
			'rest_controller_class' => 'WP_REST_Terms_Controller',
			'show_in_quick_edit'    => false,
		);
		register_taxonomy( 'project_tags', array( 'project' ), $args );
	}

	/**
	 * Custom Post Project Single Template.
	 *
	 * @since 1.0.0
	 *
	 * @param string $template   Template File URL.
	 */
	public function cpt_project_single_template( $template ) {

		if ( is_singular( 'project' ) ) {
			$template = billio_PLUGIN_DIR . 'inc/custom-post/project/templates/single-project.php';
		}

		return $template;
	}
}
