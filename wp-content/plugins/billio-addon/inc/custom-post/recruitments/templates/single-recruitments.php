<?php
/**
 * Single Recruitments Template.
 *
 * @package billio Plugins
 */

get_header();

?>

<div id="career_apply_<?php print get_the_ID(); ?>" class="career-form modal fade"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="md-description">
				<div class="heading-career-form"></div>
				<h2 class="title-career-form"><?php _e( 'Apply Now', 'billio' ); ?></h2>
				<form id="career-form" method="post" action="" enctype="multipart/form-data">
				  <div class="form-group">
					<label for="fullname"><?php _e( 'Full Name', 'billio' ); ?>:</label>
					<input type="text" name="fullname" class="form-control" id="fullname" placeholder="<?php _e( 'e.g. John Smith', 'billio' ); ?>" required>
				  </div>
				  <div class="form-group">
					<label for="email_address"><?php _e( 'Email', 'billio' ); ?>:</label>
					<input type="email" name="email_address" class="form-control" id="email_address" placeholder="<?php _e( 'e.g. john.smith@hotmail.com', 'billio' ); ?>" required>
				  </div>
				  <div class="form-group">
					<label for="note"><?php _e( 'Covering Note', 'billio' ); ?>:</label>
					<textarea class="form-control" name="note" rows="5" required></textarea>
				  </div>
				  <div class="form-group">
					<label for="file_cv"><?php _e( 'Upload CV', 'billio' ); ?>:</label>
					<input type="file" name="file_cv" id="file_cv" required>
					<p class="help-block"></p>
				  </div>
				  <button type="submit" class="btn btn-color-secondary"><?php _e( 'Apply Now', 'billio' ); ?></button>
				  <input type="hidden" name="career_id" id="career_id" value="<?php the_ID(); ?>"/>
				</form>
			</div>
			<button class="button right btn-cross md-close" data-dismiss="modal"><i class="icon-cancel"></i></button>
		 </div>
	 </div>
</div>
<div id="career_send_<?php print get_the_ID(); ?>" class="career-form modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="md-description">
				<div class="heading-career-form"></div>
				<h2 class="title-career-form"><?php _e( 'Email To a Friend', 'billio' ); ?></h2>
				<form id="career-send-form" method="post" action="">
				  <div class="form-group">
					<label for="fullname"><?php _e( 'Full Name', 'billio' ); ?>:</label>
					<input type="text" name="fullname" class="form-control" id="fullname" placeholder="<?php _e( 'e.g. John Smith', 'billio' ); ?>" required>
				  </div>
				  <div class="form-group">
					<label for="email_address"><?php _e( 'Email', 'billio' ); ?>:</label>
					<input type="email" name="email_address" class="form-control" id="email_address" placeholder="<?php _e( 'e.g. john.smith@hotmail.com', 'billio' ); ?>" required>
				  </div>
				  <div class="form-group">
					<label for="friend_email_address"><?php _e( 'Friend Email', 'billio' ); ?>:</label>
					<input type="email" name="friend_email_address" class="form-control" id="friend_email_address" placeholder="<?php _e( 'e.g. alice@hotmail.com', 'billio' ); ?>" required>
				  </div>
				  <div class="form-group">
					<label for="note"><?php _e( 'Quick Note', 'billio' ); ?>:</label>
					<textarea class="form-control" name="note" rows="5"></textarea>
					<p class="help-block"><?php _e( 'Type a quick message directed to your friend. Please avoid content that could be considered as spam as this could result in a ban from the site.', 'billio' ); ?></p>
				  </div>
				  <button type="submit" class="btn btn-color-secondary"><?php _e( 'Send Message', 'billio' ); ?></button>
				  <input type="hidden" name="career_id" id="career_id" value="<?php the_ID(); ?>"/>
				</form>
			</div>
			<button class="button md-close right btn-cross" data-dismiss="modal"><i class="icon-cancel"></i></button>
		 </div>
	  </div>
</div>

<div class="container">
	<div class="career-detail">
		<header class="uf-single-post__title" itemscope>
			<?php if ( ! has_header_image() ) : ?>
				<h1 class="uf-single-post-title entry-title" itemprop="headline"><?php the_title(); ?></h1>
			<?php endif; ?>
		</header>
	<h2><?php _e( 'Jobs Description', 'billio' ); ?></h2>
	<div class="row">
		<div class="col-sm-8<?php print is_rtl() ? ' col-sm-push-4' : ''; ?>">
			<div class="row">
				<div class="col-xs-12">
					<ul class="career-detail-list">
						<li class="career-field"><label for="job-position"><?php _e( 'Employment Type', 'billio' ); ?></label><span class="career-value"><?php print get_post_meta( get_the_ID(), '_type', true ); ?></span></li>
						<li class="career-field"><label for="job-position"><?php _e( 'Specialisms', 'billio' ); ?></label><span class="career-value"><?php print get_post_meta( get_the_ID(), '_specialist', true ); ?></span></li>
						<li class="career-field"><label for="job-position"><?php _e( 'Region', 'billio' ); ?></label><span class="career-value"><?php print get_post_meta( get_the_ID(), '_region', true ); ?></span></li>
						<li class="career-field"><label for="job-position"><?php _e( 'Location', 'billio' ); ?></label><span class="career-value"><?php print get_post_meta( get_the_ID(), '_location', true ); ?></span></li>
						<li class="career-field"><label for="job-position"><?php _e( 'Salary', 'billio' ); ?></label><span class="career-value"><?php print get_post_meta( get_the_ID(), '_salary', true ); ?></span></li>
						<li class="career-field"><label for="job-position"><?php _e( 'Start Date', 'billio' ); ?></label><span class="career-value"><?php print get_post_meta( get_the_ID(), '_startdate', true ); ?></span></li>
						<li class="career-field"><label for="job-position"><?php _e( 'End Date', 'billio' ); ?></label><span class="career-value"><?php print get_post_meta( get_the_ID(), '_enddate', true ); ?></span></li>

					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-8 career-action-button" >
					<a class="apply" data-toggle="modal" id="apply-career" data-target="#career_apply_<?php print get_the_ID(); ?>" href="javascript:;"><?php _e( 'Apply Now', 'billio' ); ?></a>
					<a class="emailtoafriend" id="send-career-to-friend"  data-toggle="modal"  data-target="#career_send_<?php print get_the_ID(); ?>" href="javascript:;"><?php _e( 'Email To a Friend', 'billio' ); ?></a>
				</div>
				<div class="col-xs-12 col-sm-4 text-right blog_info_share">
					<div class="social-share-button">
						<a href="javascript:;" class="social-share-button-hoverable"><span class="icon-forward-outline"></span></a>
						<ul class="social-share-button-group">
							<li class="facebook-button"><a href="https://www.facebook.com/sharer/sharer.php" data-url="<?php echo get_permalink(); ?>"><span><i class="billio-ss-facebook51"></i></span></a></li>
							<li class="twitter-button"><a href="https://twitter.com/intent/tweet" data-url="<?php echo get_permalink(); ?>"><span class="billio-ss-twitter44"></span></a></li>
							<li class="gplus-button"><a href="https://plus.google.com/share" data-url="<?php echo get_permalink(); ?>"><span class="billio-ss-google115"></span></a></li>  
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php wp_reset_query(); ?>
		<div class="col-sm-4<?php print is_rtl() ? ' col-sm-pull-8' : ''; ?>"><?php the_content(); ?></div>
	</div>
	</div>
</div>

<?php
get_footer();
