<?php
/**
 * Custom Posts Recruitments.
 *
 * @package billio Plugins
 */

defined( 'ABSPATH' ) || die();

/**
 * billio Plugins Custom Post Recruitments Class.
 *
 * Description.
 *
 * @since 1.0.0
 */
class billio_Plugins_Cpt_Recruitments {

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'register_billio_cpt_recruitments' ) );
		add_action( 'init', array( $this, 'register_billio_cpt_recruitments_categories' ) );
		add_filter( 'single_template', array( $this, 'cpt_recruitments_single_template' ) );
		add_theme_support( 'post-thumbnails' );
	}

	/**
	 * Register Custom Post Recruitments.
	 *
	 * @since 1.0.0
	 */
	public function register_billio_cpt_recruitments() {
		/**
		 * Post Type: Recruitments.
		 */
		$labels = array(
			'name'                  => __( 'Recruitments', 'billio-plugins' ),
			'singular_name'         => __( 'Recruitments', 'billio-plugins' ),
			'menu_name'             => __( 'Recruitments', 'billio-plugins' ),
			'all_items'             => __( 'All Recruitments', 'billio-plugins' ),
			'add_new'               => __( 'Add New', 'billio-plugins' ),
			'add_new_item'          => __( 'Add New Recruitments', 'billio-plugins' ),
			'edit_item'             => __( 'Edit Recruitments', 'billio-plugins' ),
			'new_item'              => __( 'New Recruitments', 'billio-plugins' ),
			'view_item'             => __( 'View Recruitments', 'billio-plugins' ),
			'view_items'            => __( 'View Recruitments', 'billio-plugins' ),
			'search_items'          => __( 'Search Recruitments', 'billio-plugins' ),
			'not_found'             => __( 'No Recruitments found', 'billio-plugins' ),
			'not_found_in_trash'    => __( 'No Recruitments found in trash', 'billio-plugins' ),
			'featured_image'        => __( 'Featured Image for Recruitments', 'billio-plugins' ),
			'set_featured_image'    => __( 'Set featured Image for Recruitments', 'billio-plugins' ),
			'remove_featured_image' => __( 'Remove featured image for Recruitments', 'billio-plugins' ),
			'use_featured_image'    => __( 'Use as featured Image for this Recruitments', 'billio-plugins' ),
		);

		$args = array(
			'label'                 => __( 'Recruitments', 'billio-plugins' ),
			'labels'                => $labels,
			'description'           => '',
			'public'                => true,
			'publicly_queryable'    => true,
			'show_ui'               => true,
			'delete_with_user'      => false,
			'show_in_rest'          => true,
			'rest_base'             => '',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'has_archive'           => false,
			'show_in_menu'          => true,
			'show_in_nav_menus'     => true,
			'exclude_from_search'   => false,
			'capability_type'       => 'post',
			'map_meta_cap'          => true,
			'hierarchical'          => true,
			'rewrite'               => array(
				'slug'       => 'recruitments',
				'with_front' => false,
			),
			'query_var'             => true,
			'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions', 'author', 'page-attributes' ),
			'taxonomies'            => array( 'recruitments_category' ),
			'show_in_rest'          => true,
		);

		register_post_type( 'recruitments', $args );
		add_filter( 'manage_edit-recruitments_columns', array( $this, 'billio_show_recruitments_column' ) );
		add_action( 'manage_pages_custom_column', array( $this, 'billio_port_custom_columns' ) );
	}

	/**
	 * Custom Post List.
	 *
	 * @since 1.0.0
	 *
	 * @param string $columns   Column List.
	 */
	public function billio_show_recruitments_column( $columns ) {

		$columns = array(
			'cb'                => '<input type="checkbox" />',
			'image-recruitment' => __( 'Image', 'billio-plugins' ),
			'title'             => __( 'Title', 'billio-plugins' ),
			'author'            => __( 'Author', 'billio-plugins' ),
			'dtpost-category'   => __( 'Categories', 'billio-plugins' ),
			'date'              => __( 'Date', 'billio-plugins' ),
		);
		return $columns;
	}

	/**
	 * Add Image Column to Custom Post List.
	 *
	 * @since 1.0.0
	 *
	 * @param string $column   Image at First Column List.
	 */
	public function billio_port_custom_columns( $column ) {

		global $post;
		switch ( $column ) {
			case 'recruitments-category':
				echo get_the_term_list( $post->ID, 'recruitments_category', '', ', ', '' );
				break;
			case 'image-recruitment':
				$attachment_id = get_the_post_thumbnail( $post->ID, 'thumbnail' );
				print ( $attachment_id ) ? $attachment_id : '';
				break;
		}
	}

	/**
	 * Register Custom Post Recruitments Categories.
	 *
	 * @since 1.0.0
	 */
	public function register_billio_cpt_recruitments_categories() {
		/**
		 * Taxonomy: Recruitments Categories.
		 */
		$labels = array(
			'name'          => __( 'Recruitments Categories', 'billio-plugins' ),
			'singular_name' => __( 'Recruitments Category', 'billio-plugins' ),
			'not_found'     => __( 'No category found', 'billio-plugins' ),
		);

		$args = array(
			'label'                 => __( 'Recruitments Categories', 'billio-plugins' ),
			'labels'                => $labels,
			'public'                => true,
			'publicly_queryable'    => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'show_in_nav_menus'     => true,
			'query_var'             => true,
			'rewrite'               => array(
				'slug'       => 'recruitments_category',
				'with_front' => true,
			),
			'show_admin_column'     => false,
			'show_in_rest'          => true,
			'rest_base'             => 'recruitments_category',
			'rest_controller_class' => 'WP_REST_Terms_Controller',
			'show_in_quick_edit'    => false,
		);
		register_taxonomy( 'recruitments_category', array( 'recruitments' ), $args );
	}

	/**
	 * Custom Post Recruitments Single Template.
	 *
	 * @since 1.0.0
	 *
	 * @param string $template   Template File URL.
	 */
	public function cpt_recruitments_single_template( $template ) {

		if ( is_singular( 'recruitments' ) ) {
			$template = billio_PLUGIN_DIR . 'inc/custom-post/recruitments/templates/single-recruitments.php';
		}

		return $template;
	}
}
