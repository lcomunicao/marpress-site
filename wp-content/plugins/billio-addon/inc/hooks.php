<?php
/**
 * The Hooks
 *
 * @package billio
 */

if ( ! function_exists( 'billio_plugin_enqueue_scripts' ) ) :

	/**
	 * Enqueue plugin styles.
	 */
	function billio_plugin_enqueue_scripts() {
		wp_register_script( 'billio-plugins-script', billio_PLUGIN_URL . 'assets/js/plugins.min.js', array( 'jquery' ), null, true );
		wp_register_style( 'billio-plugins-style', billio_PLUGIN_URL . 'assets/css/plugins.min.css' );
		wp_register_style( 'simple-line-icons', billio_PLUGIN_URL . 'assets/css/simple-line-icons.css' );
		wp_register_style( 'social-icons', billio_PLUGIN_URL . 'assets/css/social-icons.css' );
		wp_register_style( 'billio-flaticon', billio_PLUGIN_URL . 'assets/css/flaticon.css' );
		wp_register_style( 'billio_report_post_icons', billio_PLUGIN_URL . 'assets/css/flaticon-report.css' );
		wp_enqueue_style( 'billio_report_post_icons' );

		wp_enqueue_style( 'billio-plugins-style' );
		wp_enqueue_style( 'social-icons' );
		wp_enqueue_style( 'simple-line-icons' );
		wp_enqueue_style( 'billio-flaticon' );
		wp_enqueue_script( 'billio-plugins-script' );

		wp_register_script( 'billio-career-reply', billio_PLUGIN_URL . 'assets/js/career.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'billio-career-reply' );

		/**
		 * Load Vendor Icon Picker.
		 */
		if ( 'genericon' === billio_plugin_setting( 'test-icon-picker', true, 'iconset' ) ) {
			wp_enqueue_style( 'genericons', get_theme_file_uri( '/inc/customizer/assets/vendor/icon-picker/css/genericons/genericons.css' ), array(), null );
		}
		if ( 'dashicon' === billio_plugin_setting( 'test-icon-picker', true, 'iconset' ) ) {
			wp_enqueue_style( 'dashicons' );
		}
		if ( 'fa' === billio_plugin_setting( 'test-icon-picker', true, 'iconset' ) ) {
			wp_enqueue_style( 'font-awesome', get_theme_file_uri( '/inc/customizer/assets/vendor/icon-picker/css/font-awesome/css/font-awesome.min.css' ), array(), null );
		}
	}

	add_action( 'wp_enqueue_scripts', 'billio_plugin_enqueue_scripts' );
endif;

if ( ! function_exists( 'billio_get_pages_array' ) ) :

	/**
	 * Get Pages Array.
	 *
	 * @param array $args get array of args.
	 * @return $result Array
	 */
	function billio_get_pages_array( $args = array() ) {
		$result = array();
		$pages  = get_pages( $args );
		if ( ! empty( $pages ) ) {
			foreach ( $pages as $page ) {
				$result[ $page->ID ] = $page->post_title;
			}
		}

		return $result;
	}
endif;

if ( ! function_exists( 'billio_display_nav_builder' ) ) :

	/**
	 * Display a selected page on footer.
	 *
	 * @return $result string
	 */
	function billio_display_nav_builder() {
		$postid  = get_the_ID();
		$navpage = get_theme_mod( 'billio-nav-content' );

		// if ( get_theme_mod( 'footer-option', 'footer-widget' ) == 'footer-widget' || ! $footerpage || $postid == $footerpage ) {
		// return;
		// }
		echo apply_filters( 'the_content', get_post_field( 'post_content', $navpage ) );
	}

	add_action( 'billio_display_nav_builder', 'billio_display_nav_builder' );
endif;

if ( ! function_exists( 'billio_plugin_admin_enqueue_scripts' ) ) :

	/**
	 * Enqueue plugin styles.
	 */
	function billio_plugin_admin_enqueue_scripts() {
		wp_register_style( 'simple-line-icons', billio_PLUGIN_URL . 'assets/css/simple-line-icons.css' );
		wp_register_style( 'social-icons', billio_PLUGIN_URL . 'assets/css/social-icons.css' );
		wp_register_script( 'billio-plugins-script', billio_PLUGIN_URL . 'assets/js/plugins.min.js', array( 'jquery' ), null, true );

		wp_enqueue_style( 'social-icons' );
		wp_enqueue_style( 'simple-line-icons' );
		wp_enqueue_script( 'billio-plugins-script' );
	}

	add_action( 'admin_enqueue_scripts', 'billio_plugin_admin_enqueue_scripts' );
endif;

add_action( 'wp_head', 'myplugin_ajaxurl' );

function myplugin_ajaxurl() {

	echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url( 'admin-ajax.php' ) . '";
         </script>';
}

if ( ! function_exists( 'billio_proccess_apply_career' ) ) :
	function billio_proccess_apply_career() {

		global $detheme_config;
		$career_id       = intVal( $_POST['career_id'] );
		$fullname        = sanitize_text_field( $_POST['fullname'] );
		$email           = sanitize_email( $_POST['email'] );
		$notes           = esc_textarea( $_POST['notes'] );
		$recipient       = esc_attr( billio_theme_setting( 'billio-recruitments-email-target' ) );
		$attachment      = $_FILES['attachment'];
		$thankyoumessage = esc_attr( billio_theme_setting( 'billio-recruitments-confirmation-message' ) );
		// $career_attach_type=isset($detheme_config['career_attach_type'])?$detheme_config['career_attach_type']:false;
		if ( $recipient == '' ) {

			$super_admins = get_super_admins();
			foreach ( $super_admins as $admin ) {
				$adminuser   = get_user_by( 'login', $admin );
				$recipient[] = $adminuser->user_email;
			}
		}

		$career = get_post( $career_id );

		if ( ! $career ) {
			print json_encode( array( 'error' => __( 'Job position not found or closed', 'billio_plugin' ) ) );
			die();

		}

		$from_email  = get_bloginfo( 'admin_email' );
		$attachlimit = ( isset( $detheme_config['career_attach_limit'] ) && '' != $detheme_config['career_attach_limit'] ) ? $detheme_config['career_attach_limit'] : 1024;

		if ( $attachment ) {

			// if($attachment['size'] > $attachlimit*1024){
			// print json_encode(array('error'=>__('Attachment size exceed allowed','billio_plugin')));
			// die();
			// }
			// if($career_attach_type){
			// $allowed=false;
			foreach ( array_keys( $career_attach_type ) as $key ) {
				if ( wp_match_mime_types( $key, $attachment['type'] ) ) {
					$allowed = true;
					break;
				}
			}

			// if(!$allowed){
			// print json_encode(array('error'=>sprintf(__('%s not allowed','billio_plugin'),$attachment['type'])));
			// die();
			// }
			// }else{
			// print json_encode(array('error'=>__('Attachment type not allowed','billio_plugin')));
			// die();
			// }
			add_action( 'phpmailer_init', 'billio_get_career_attachment' );
		}

		$subject  = sprintf( __( 'Apply Job for Position %s', 'billio_plugin' ), ucwords( $career->post_name ) );
		$headers  = 'From: ' . stripslashes_deep( html_entity_decode( $fullname, ENT_COMPAT, 'UTF-8' ) ) . " <$from_email>\r\n";
		$headers .= 'Reply-To: ' . $email . "\r\n";
		$headers .= 'Return-Path: ' . $email . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "X-Priority: 1\r\n";

		// if ( $use_html ) {
		$headers .= "Content-Type: text/html\n";
		// }
		$notes .= "\n\n" . sprintf( __( 'This application for job position %s', 'billio_plugin' ), '<a href="' . get_permalink( $career_id ) . '">' . $career->post_name . '</a>' );

		$body = wpautop( $notes );

		$sendmail = wp_mail( $recipient, $subject, $body, $headers );

		if ( $sendmail ) {
			print json_encode( array( 'success' => $thankyoumessage ) );
		} else {
			print json_encode( array( 'error' => __( 'Could not instantiate mail function', 'billio_plugin' ) ) );
		}
		die();
	}
  endif; // if ( ! function_exists( 'billio_proccess_apply_career' ) )

  add_action( 'wp_ajax_billio_apply_career', 'billio_proccess_apply_career' );
  add_action( 'wp_ajax_nopriv_billio_apply_career', 'billio_proccess_apply_career' );

  /**
   * Billio Get Career Attachment.
   */
function billio_get_career_attachment( $phpmailer ) {

	$attachment = $_FILES['attachment'];

	$filesize   = $attachment['size'];
	$filesource = $attachment['tmp_name'];
	$filename   = $attachment['name'];

	try {
		$phpmailer->AddAttachment( $filesource, $filename );
	} catch ( phpmailerException $e ) {

	}
}

if ( ! function_exists( 'billio_proccess_send_friend_career' ) ) :

	/**
	 * Billio Proccess Send Friend Career.
	 */
	function billio_proccess_send_friend_career() {

		global $detheme_config;
		$career_id       = intVal( $_POST['career_id'] );
		$fullname        = sanitize_text_field( $_POST['fullname'] );
		$email           = sanitize_email( $_POST['email'] );
		$recipient       = sanitize_email( $_POST['email_to'] );
		$notes           = esc_textarea( $_POST['notes'] );
		$thankyoumessage = __( 'Your email has been sent', 'billio_plugin' );

		$career = get_post( $career_id );

		if ( ! $career ) {
			print json_encode( array( 'error' => __( 'Job position not found or closed', 'billio_plugin' ) ) );
			die();

		}

		$from_email = $email;

		$subject  = sprintf( __( '%1$s tell you about job for Position %2$s', 'billio_plugin' ), ucfirst( $fullname ), ucwords( $career->post_name ) );
		$headers  = 'From: ' . stripslashes_deep( html_entity_decode( $fullname, ENT_COMPAT, 'UTF-8' ) ) . " <$from_email>\r\n";
		$headers .= 'Reply-To: ' . $email . "\r\n";
		$headers .= 'Return-Path: ' . $email . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "X-Priority: 1\r\n";

		$headers .= "Content-Type: text/html\n";

		$body = sprintf( __( "Hi,\nYour friend %1\$s about job position %2\$s.\n", 'billio_plugin' ), $fullname, '<a href="' . get_permalink( $career_id ) . '">' . $career->post_name . '</a>' ) .
		"\n\n" .
		wpautop( $notes );

		$sendmail = wp_mail( $recipient, $subject, $body, $headers );

		if ( $sendmail ) {
			print json_encode( array( 'success' => $thankyoumessage ) );
		} else {
			print json_encode( array( 'error' => __( 'Could not instantiate mail function', 'billio_plugin' ) ) );
		}
			die();

	}
  endif; // if ( ! function_exists( 'billio_proccess_send_friend_career' ) ).

  add_action( 'wp_ajax_billio_send_friend_career', 'billio_proccess_send_friend_career' );
  add_action( 'wp_ajax_nopriv_billio_send_friend_career', 'billio_proccess_send_friend_career' );

if ( ! function_exists( 'billio_metabox_hide_title' ) ) :

	/**
	 * Billio Hide Title Metabox
	 */
	function billio_metabox_hide_title() {
		add_meta_box(
			'vast-meta-hide-title-id', __( 'Billio Options', 'billio-plugins' ), 'billio_metabox_hide_title_control', 'page', 'normal', 'high', array(
				'__block_editor_compatible_meta_box' => true,
				'__back_compat_meta_box'             => false,
			)
		);
	}

endif;

if ( ! function_exists( 'billio_metabox_hide_title_control' ) ) :

	/**
	 * Billio hide title checkbox
	 */
	function billio_metabox_hide_title_control() {
		global $post;
		wp_nonce_field( 'billio_nonce', 'billio_hide_title_nonce' );
		$is_hide = get_post_meta( $post->ID, '_hide_title', true );
		?>
		<p><input type="checkbox" name="hide_title" id="hide_title" value="1" <?php echo esc_attr( ( $is_hide ) ? 'checked="checked"' : '' ); ?>/> <?php esc_html_e( 'Hide Title', 'billio-plugins' ); ?></strong></p>
		<?php
	}

endif;

if ( ! function_exists( 'billio_metabox_hide_title_save' ) ) :

	/**
	 * Vast hide title checkbox.
	 *
	 * @param string $post_id Post_id.
	 * @param post   $post The post object.
	 * @param bool   $update Whether this is an existing post being updated or not.
	 */
	function billio_metabox_hide_title_save( $post_id, $post, $update ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		if ( ! empty( $_REQUEST['billio_hide_title_nonce'] ) ) {
			if ( ! wp_verify_nonce( $_REQUEST['billio_hide_title_nonce'], 'billio_nonce' ) ) {
				return;
			}
		}

		if ( $update ) {
			$old = get_post_meta( $post_id, '_hide_title', true );
			$new = isset( $_POST['hide_title'] ) ? sanitize_text_field( $_POST['hide_title'] ) : '';

			update_post_meta( $post_id, '_hide_title', $new, $old );
		}
	}

	add_action( 'save_post', 'billio_metabox_hide_title_save', 10, 3 );

endif;

if ( ! function_exists( 'billio_metabox_recruitments' ) ) :

	/**
	 * Billio Hide Title Metabox
	 */
	function billio_metabox_recruitments() {
		add_meta_box(
			'billio-meta-recruitments-id', __( 'Career Jobs', 'billio-plugins' ), 'billio_metabox_recruitments_control', 'recruitments', 'side', 'high', array(
				'__block_editor_compatible_meta_box' => true,
				'__back_compat_meta_box'             => false,
			)
		);
	}

endif;

if ( ! function_exists( 'billio_metabox_recruitments_control' ) ) :

	/**
	 * Billio hide title checkbox
	 */
	function billio_metabox_recruitments_control() {
		global $post;
		wp_nonce_field( 'billio_nonce', 'billio_recruitments_nonce' );
		$type       = get_post_meta( $post->ID, '_type', true );
		$specialist = get_post_meta( $post->ID, '_specialist', true );
		$region     = get_post_meta( $post->ID, '_region', true );
		$location   = get_post_meta( $post->ID, '_location', true );
		$salary     = get_post_meta( $post->ID, '_salary', true );
		$startdate  = get_post_meta( $post->ID, '_startdate', true );
		$enddate    = get_post_meta( $post->ID, '_enddate', true );
		?>
		<table>
		<tr><td><?php esc_html_e( 'Employment Type', 'billio-addon' ); ?> </td><td><input type="text" name="type" id="type" value="<?php echo esc_attr( ( $type ) ? $type : '' ); ?>" /></td><tr>
		<tr><td><?php esc_html_e( 'Specialism', 'billio-addon' ); ?> </td><td><input type="text" name="specialist" id="specialist" value="<?php echo esc_attr( ( $specialist ) ? $specialist : '' ); ?>" /></td><tr>
		<tr><td><?php esc_html_e( 'Region', 'billio-addon' ); ?> </td><td><input type="text" name="region" id="region" value="<?php echo esc_attr( ( $region ) ? $region : '' ); ?>" /></td><tr>
		<tr><td><?php esc_html_e( 'Location', 'billio-addon' ); ?> </td><td><input type="text" name="location" id="location" value="<?php echo esc_attr( ( $location ) ? $location : '' ); ?>" /></td><tr>
		<tr><td><?php esc_html_e( 'Salary', 'billio-addon' ); ?> </td><td><input type="text" name="salary" id="salary" value="<?php echo esc_attr( ( $salary ) ? $salary : '' ); ?>" /></td><tr>
		<tr><td><?php esc_html_e( 'Start Date', 'billio-addon' ); ?> </td><td><input type="text" name="startdate" id="startdate" value="<?php echo esc_attr( ( $startdate ) ? $startdate : '' ); ?>" /></td><tr>
		<tr><td><?php esc_html_e( 'End Date', 'billio-addon' ); ?> </td><td><input type="text" name="enddate" id="enddate" value="<?php echo esc_attr( ( $enddate ) ? $enddate : '' ); ?>" /></td><tr>
		</table>
		<?php
	}

endif;

if ( ! function_exists( 'billio_metabox_recruitments_save' ) ) :

	/**
	 * Vast Recruitment.
	 *
	 * @param string $post_id Post_id.
	 * @param post   $post The post object.
	 * @param bool   $update Whether this is an existing post being updated or not.
	 */
	function billio_metabox_recruitments_save( $post_id, $post, $update ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		if ( ! empty( $_REQUEST['billio_recruitments_nonce'] ) ) {
			if ( ! wp_verify_nonce( $_REQUEST['billio_recruitments_nonce'], 'billio_nonce' ) ) {
				return;
			}
		}

		if ( $update ) {
			$old = get_post_meta( $post_id, '_type', true );
			$new = isset( $_POST['type'] ) ? sanitize_text_field( $_POST['type'] ) : '';
			update_post_meta( $post_id, '_type', $new, $old );
			$old = get_post_meta( $post_id, '_specialist', true );
			$new = isset( $_POST['specialist'] ) ? sanitize_text_field( $_POST['specialist'] ) : '';
			update_post_meta( $post_id, '_specialist', $new, $old );
			$old = get_post_meta( $post_id, '_region', true );
			$new = isset( $_POST['region'] ) ? sanitize_text_field( $_POST['region'] ) : '';
			update_post_meta( $post_id, '_region', $new, $old );
			$old = get_post_meta( $post_id, '_location', true );
			$new = isset( $_POST['location'] ) ? sanitize_text_field( $_POST['location'] ) : '';
			update_post_meta( $post_id, '_location', $new, $old );
			$old = get_post_meta( $post_id, '_salary', true );
			$new = isset( $_POST['salary'] ) ? sanitize_text_field( $_POST['salary'] ) : '';
			update_post_meta( $post_id, '_salary', $new, $old );
			$old = get_post_meta( $post_id, '_startdate', true );
			$new = isset( $_POST['startdate'] ) ? sanitize_text_field( $_POST['startdate'] ) : '';
			update_post_meta( $post_id, '_startdate', $new, $old );
			$old = get_post_meta( $post_id, '_enddate', true );
			$new = isset( $_POST['enddate'] ) ? sanitize_text_field( $_POST['enddate'] ) : '';
			update_post_meta( $post_id, '_enddate', $new, $old );

		}
	}

	add_action( 'save_post', 'billio_metabox_recruitments_save', 10, 3 );

endif;

/**
 * Your New Function
 */
function your_new_function() {
	echo 'HOHOHO <input name="{{data.name}}" class="billio-param" value="{{data.value}}" type="text" />';
}

if ( ! function_exists( 'is_plugin_active' ) ) {
	include_once ABSPATH . 'wp-admin/includes/plugin.php';
}

if ( ! function_exists( 'billio_init_map' ) ) :

	if ( is_plugin_active( 'kingcomposer/kingcomposer.php' ) ) {
		add_action( 'init', 'billio_init_map', 99 );
	}

	/**
	 * Billio Init Map
	 */
	function billio_init_map() {

		global $kc;
		$kc->set_template_path( billio_PLUGIN_DIR . '/kingcomposer/' );
		$kc->add_param_type( 'new_param', 'your_new_function' );

		if ( function_exists( 'kc_add_map' ) ) {
			kc_add_map(
				array(
					'billio_view' => array(
						'name'        => 'Billio Document Viewer',
						'description' => __( '', 'billio-plugins' ),
						'icon'        => 'fa-search',
						'category'    => 'Billio',
						'params'      => array(
							array(
								'name'        => 'title',
								'label'       => 'title',
								'type'        => 'text',
								'admin_label' => false,
								'description' => '',
							),
							array(
								'name'        => 'file',
								'label'       => 'Select File',
								'type'        => 'attach_image',
								'admin_label' => false,
								'description' => '',
							),
							array(
								'name'        => 'height',
								'label'       => 'Frame Height (px)',
								'type'        => 'text',
								'value'       => '500',
								'admin_label' => false,
								'description' => '',
							),
							array(
								'name'    => 'download',
								'label'   => 'Show Download Button',
								'type'    => 'radio',
								'options' => array(
									'yes' => 'Yes',
									'no'  => 'No',
								),
								'value'   => 'yes',
							),
						),
					),  // End of elemnt kc_icon.
				)
			); // End add map.

			kc_add_map(
				array(
					'billio_media_download' => array(
						'name'         => __( 'Media Download', 'billio-plugins' ),
						'title'        => 'Nested Title',
						'icon'         => 'fa-star',
						'category'     => 'Billio',

						'nested'       => true,

						'accept_child' => 'billio_media',
						// 'except_child'    => '',
						// Use only accept_child or except_child to set which elements able to added
						// 'accept_parent'	=> '',
						// 'except_parent'  => '',
						'params'       => array(
							array(
								'type'  => 'text',
								'label' => __( 'Label', 'billio-plugins' ),
								'name'  => 'media_download',
								// 'admin_label'	=> true,
							),
						),
					),
				)
			); // End add map.

			kc_add_map(
				array(
					'billio_media' => array(
						'name'        => 'Media',
						'description' => __( '', 'billio-plugins' ),
						'icon'        => 'fa-search',
						'category'    => 'Billio',
						'params'      => array(
							array(
								'name'        => 'title',
								'label'       => 'title',
								'type'        => 'text',
								'admin_label' => false,
								'description' => '',
							),
							array(
								'name'        => 'file',
								'label'       => 'Choose File',
								'type'        => 'attach_image',
								'admin_label' => false,
								'description' => '',
							),
						),
					),  // End of elemnt kc_icon.
				)
			); // End add map.

		} // End if.

	}
endif;

if ( ! function_exists( 'billio_metabox_reports' ) ) :

	/**
	 * Billio Document Options Metabox
	 */
	function billio_metabox_reports() {
		add_meta_box(
			'billio-meta-reports-id', __( 'Document Options', 'billio-plugins' ), 'billio_metabox_reports_control', 'reports', 'side', 'high', array(
				'__block_editor_compatible_meta_box' => true,
				'__back_compat_meta_box'             => false,
			)
		);
	}

endif;

if ( ! function_exists( 'billio_metabox_reports_control' ) ) :

	/**
	 * Billio Document Options Metabox
	 *
	 * @param post $post The post object.
	 */
	function billio_metabox_reports_control( $post ) {
		global $post;
		wp_nonce_field( 'billio_nonce', 'billio_reports_nonce' );

		$pre_title          = get_post_meta( $post->ID, '_pre_title', true );
		$button_label       = get_post_meta( $post->ID, '_button_label', true );
		$document_url       = get_post_meta( $post->ID, '_document_url', true );
		$document_extension = get_post_meta( $post->ID, '_document_extension', true );
		$document_filename  = pathinfo( $document_url, PATHINFO_FILENAME );
		$document_icon      = get_post_meta( $post->ID, '_document_icon', true );

		?>
		<script>
		jQuery(document).ready( function($) {
			$('#upload_document_link').click(
				function() {
					formfield = jQuery('#document_url').attr('name');
					tb_show('', 'media-upload.php?type=file&amp;TB_iframe=true' );
					return false;
				}
			);
				
			window.send_to_editor = function(html) {
				var doc_icon = '';
				var doc_extension = '';
				var doc_url =  jQuery(html).attr('href')
				var doc_filename = doc_url.split('/').pop();
				var doc_extension = doc_url.split('.').pop();
				alert(doc_extension);
				$('#document_name a').attr('href',doc_url);
				$('#document_name a span').html(doc_filename);

				$('#document_url').val(doc_url);

				switch(doc_extension) {
					case 'ai':
						doc_icon = "flaticondr-ai1";
						break;
					case 'docx':
						doc_icon = "flaticondr-docx1";
						break;
					case 'html':
						doc_icon = "flaticondr-html8";
						break;
					case 'jpg':
						doc_icon = "flaticondr-jpg2";
						break;
					case 'jpeg':
						doc_icon = "flaticondr-jpg2";
						break;
					case 'mp3':
						doc_icon = "flaticondr-mp34";
						break;
					case 'mp4':
						doc_icon = "flaticondr-mp42";
						break;
					case 'pdf':
						doc_icon = "flaticondr-pdf17";
						break;
					case 'psd':
						doc_icon = "flaticondr-photoshop";
						break;
					case 'png':
						doc_icon = "flaticondr-png4";
						break;
					case 'ppt':
						doc_icon = "flaticondr-ppt2";
						break;
					case 'pptx':
						doc_icon = "flaticondr-pptx";
						break;
					case 'rar':
						doc_icon = "flaticondr-rar";
						break;
					case 'txt':
						doc_icon = "flaticondr-txt";
						break;
					case 'xls':
						doc_icon = "flaticondr-xls2";
						break;
					case 'xlsx':
						doc_icon = "flaticondr-xlsx1";
						break;
					case 'xml':
						doc_icon = "flaticondr-xml6";
						break;
					case 'zip':
						doc_icon = "flaticondr-zip5";
						break;
					case '':
						doc_icon = "";
						break;
					default:
						doc_icon = "flaticondr-doc";
				}

				$('#document_icon').attr('class','');
				$('#document_icon').addClass(doc_icon);

				tb_remove();
			}

			$('#remove_document_link').click(
				function() {
					$('#document_url').val('');
					$('#document_name a').attr('href','javascript:;');
					$('#document_name a span').html('');
					
					$('#document_icon').attr('class','');
				}
			);
		});
		</script>
	  
		<div class="media-upload">
		<div><?php _e( 'Pre Title', 'billio-plugins' ); ?></div>
		<input type="text" id="pre_title" name="pre_title" value="<?php echo esc_attr( $pre_title ); ?>" /><br /><br />
		
		<div><?php _e( 'Document File', 'billio-plugins' ); ?></div>
		
		<div id="document_name"><a href="<?php echo esc_url( $document_url ); ?>"><i id="document_icon" class="<?php echo esc_attr( $document_icon ); ?>"></i><br /><span><?php echo esc_html( $document_filename . $document_extension ); ?></span></a></div>
		
		<a href="javascript:;" id="upload_document_link"><?php _e( 'Upload Document', 'billio-plugins' ); ?></a> - <a href="javascript:;" id="remove_document_link"><?php _e( 'Remove Document', 'billio_report_post' ); ?></a>
		<input type="hidden" id="document_url" name="document_url" value="<?php echo esc_attr( $document_url ); ?>" /><br /><br />
		
		<div><?php _e( 'Download Button Label', 'billio-plugins' ); ?></div>
		<input type="text" id="button_label" name="button_label" value="<?php echo esc_attr( $button_label ); ?>" />
	</div>
	<?php
	}
endif;

if ( ! function_exists( 'billio_metabox_reports_save' ) ) :

	/**
	 * Vast Recruitment.
	 *
	 * @param string $post_id Post_id.
	 * @param post   $post The post object.
	 * @param bool   $update Whether this is an existing post being updated or not.
	 */
	function billio_metabox_reports_save( $post_id, $post, $update ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		if ( ! empty( $_REQUEST['billio_reports_nonce'] ) ) {
			if ( ! wp_verify_nonce( $_REQUEST['billio_reports_nonce'], 'billio_nonce' ) ) {
				return;
			}
		}

		if ( $update ) {
			if ( isset( $_POST['document_url'] ) ) {
				$document_extension = pathinfo( $_POST['document_url'], PATHINFO_EXTENSION );
				update_post_meta( $post_id, '_document_icon', billio_get_report_icon( $document_extension ) );
			}

			$old = get_post_meta( $post_id, '_pre_title', true );
			$new = isset( $_POST['pre_title'] ) ? sanitize_text_field( $_POST['pre_title'] ) : '';
			update_post_meta( $post_id, '_pre_title', $new, $old );

			$old = get_post_meta( $post_id, '_button_label', true );
			$new = isset( $_POST['button_label'] ) ? sanitize_text_field( $_POST['button_label'] ) : '';
			update_post_meta( $post_id, '_button_label', $new, $old );

			$old = get_post_meta( $post_id, '_document_url', true );
			$new = isset( $_POST['document_url'] ) ? sanitize_text_field( $_POST['document_url'] ) : '';
			update_post_meta( $post_id, '_document_url', $new, $old );

			$old = get_post_meta( $post_id, '_document_extension', true );
			$new = isset( $document_extension ) ? $document_extension : '';
			update_post_meta( $post_id, '_document_extension', $new, $old );

		}

	}

	add_action( 'save_post', 'billio_metabox_reports_save', 10, 3 );

endif;

if ( ! function_exists( 'billio_get_report_icon' ) ) :

	/**
	 * Billio get report icon.
	 *
	 * @param string $doc_ext Doc Extension.
	 */
	function billio_get_report_icon( $doc_ext ) {
		$document_icon = '';

		switch ( $doc_ext ) {
			case 'ai':
				$document_icon = 'flaticondr-ai1';
				break;
			case 'docx':
				$document_icon = 'flaticondr-docx1';
				break;
			case 'html':
				$document_icon = 'flaticondr-html8';
				break;
			case 'jpg':
				$document_icon = 'flaticondr-jpg2';
				break;
			case 'jpeg':
				$document_icon = 'flaticondr-jpg2';
				break;
			case 'mp3':
				$document_icon = 'flaticondr-mp34';
				break;
			case 'mp4':
				$document_icon = 'flaticondr-mp42';
				break;
			case 'pdf':
				$document_icon = 'flaticondr-pdf17';
				break;
			case 'psd':
				$document_icon = 'flaticondr-photoshop';
				break;
			case 'png':
				$document_icon = 'flaticondr-png4';
				break;
			case 'ppt':
				$document_icon = 'flaticondr-ppt2';
				break;
			case 'pptx':
				$document_icon = 'flaticondr-pptx';
				break;
			case 'rar':
				$document_icon = 'flaticondr-rar';
				break;
			case 'txt':
				$document_icon = 'flaticondr-txt';
				break;
			case 'xls':
				$document_icon = 'flaticondr-xls2';
				break;
			case 'xlsx':
				$document_icon = 'flaticondr-xlsx1';
				break;
			case 'xml':
				$document_icon = 'flaticondr-xml6';
				break;
			case 'zip':
				$document_icon = 'flaticondr-zip5';
				break;
			case '':
				$document_icon = '';
				break;
			default:
				$document_icon = 'flaticondr-doc';
		}

		return $document_icon;
	}
endif;

if ( ! function_exists( 'billio_body_class' ) ) :

	/**
	 * Billio hide title body class.
	 *
	 * @param array $classes Body Class.
	 */
	function billio_body_class( $classes ) {
		global $post;

		if ( $post ) {
			$is_hide = get_post_meta( $post->ID, '_hide_title', true );
			if ( $is_hide ) {
				$classes[] = 'hide-title';
			}
		}
		$classes[] = 'billio-menu-stretch' === get_theme_mod( 'billio-nav-layout' ) ? 'billio_menu_stretch' : '';

		$classes[] = 'billio-menu-center' === get_theme_mod( 'billio-nav-layout' ) ? 'billio_menu_center' : '';

		$classes[] = 'on' === get_theme_mod( 'billio-box-arround' ) ? 'billio_box_arround' : '';

		return $classes;
	}

	add_filter( 'body_class', 'billio_body_class' );
endif;

if ( is_admin() ) {
	add_action( 'add_meta_boxes', 'billio_metabox_hide_title' );
	add_action( 'add_meta_boxes', 'billio_metabox_recruitments' );
	add_action( 'add_meta_boxes', 'billio_metabox_reports' );
}

if ( ! function_exists( 'billio_metabox_hide_breadcrumbs' ) ) :

	/**
	 * Billio Hide Breadcrumbs Metabox
	 */
	function billio_metabox_hide_breadcrumbs() {
		add_meta_box(
			'vast-meta-hide-breadcrumbs-id', __( 'billio Breadcrumbs Options', 'billio-plugins' ), 'billio_metabox_hide_breadcrumbs_control', 'page', 'normal', 'high', array(
				'__block_editor_compatible_meta_box' => true,
				'__back_compat_meta_box'             => false,
			)
		);
	}

	endif;

if ( ! function_exists( 'billio_metabox_hide_breadcrumbs_control' ) ) :

	/**
	 * Billio hide breadcrumbs checkbox
	 */
	function billio_metabox_hide_breadcrumbs_control() {
		global $post;
		wp_nonce_field( 'billio_nonce', 'billio_hide_breadcrumbs_nonce' );
		$is_hide = get_post_meta( $post->ID, '_hide_breadcrumbs', true );

		$as_customizer = get_post_meta( $post->ID, '_as_customizer_breadcrumbs', true );
		$as_customizer = ( empty( $as_customizer ) && ( '0' !== $as_customizer ) ) ? '1' : $as_customizer;
		?>
		<p><input type="checkbox" name="as_customizer_breadcrumbs" id="as_customizer_breadcrumbs" value="1" <?php echo esc_attr( ( '1' === $as_customizer ) ? 'checked="checked"' : '' ); ?>/> <?php esc_html_e( 'Same as Customizer Setting', 'billio-plugins' ); ?></strong></p>
		<p><input type="checkbox" name="hide_breadcrumbs" id="hide_breadcrumbs" value="1" <?php echo esc_attr( ( $is_hide ) ? 'checked="checked"' : '' ); ?> <?php echo esc_attr( ( '1' === $as_customizer ) ? 'disabled="disabled"' : '' ); ?>/> <?php esc_html_e( 'Hide Breadcrumbs', 'billio-plugins' ); ?></strong></p>
		<?php
	}

	endif;

if ( ! function_exists( 'billio_metabox_hide_breadcrumbs_save' ) ) :

	/**
	 * Vast hide breadcrumbs checkbox.
	 *
	 * @param string $post_id Post_id.
	 * @param post   $post The post object.
	 * @param bool   $update Whether this is an existing post being updated or not.
	 */
	function billio_metabox_hide_breadcrumbs_save( $post_id, $post, $update ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		if ( ! empty( $_REQUEST['billio_hide_breadcrumbs_nonce'] ) ) {
			if ( ! wp_verify_nonce( $_REQUEST['billio_hide_breadcrumbs_nonce'], 'billio_nonce' ) ) {
				return;
			}
		}

		if ( $update ) {
			$old = get_post_meta( $post_id, '_as_customizer_breadcrumbs', true );
			$new = isset( $_POST['as_customizer_breadcrumbs'] ) ? sanitize_text_field( $_POST['as_customizer_breadcrumbs'] ) : '0';

			update_post_meta( $post_id, '_as_customizer_breadcrumbs', $new, $old );

			$old = get_post_meta( $post_id, '_hide_breadcrumbs', true );
			$new = isset( $_POST['hide_breadcrumbs'] ) ? sanitize_text_field( $_POST['hide_breadcrumbs'] ) : '';

			update_post_meta( $post_id, '_hide_breadcrumbs', $new, $old );
		}
	}

	add_action( 'save_post', 'billio_metabox_hide_breadcrumbs_save', 10, 3 );

	endif;

if ( is_admin() ) {
	add_action( 'add_meta_boxes', 'billio_metabox_hide_breadcrumbs' );
}

if ( ! function_exists( 'billio_addon_hide_breadcrumbs' ) ) :

	/**
	 * Billio hide breadcrumbs filter.
	 *
	 * @param bool $hide_breadcrumbs Hide Breadcrumbs state.
	 */
	function billio_addon_hide_breadcrumbs( $hide_breadcrumbs ) {
		global $post;

		if ( $post ) {
			$is_hide       = get_post_meta( $post->ID, '_hide_breadcrumbs', true );
			$as_customizer = get_post_meta( $post->ID, '_as_customizer_breadcrumbs', true );
			if ( '1' === $as_customizer ) {
				return $hide_breadcrumbs;
			} else {
				$hide_breadcrumbs = ( '1' === $is_hide ) ? true : false;
			}
		}

		return $hide_breadcrumbs;
	}

	add_filter( 'hide_breadcrumbs', 'billio_addon_hide_breadcrumbs' );
endif;

if ( ! function_exists( 'register_foo_widget' ) ) :

	/**
	 * Register Widget.
	 */
	function register_foo_widget() {
		register_widget( 'Foo_Widget' );

		register_sidebar(
			array(
				'name'          => esc_html__( 'Menu Info', 'billio' ),
				'id'            => 'nav-infobox',
				'description'   => esc_html__( 'Widgets in this area will be shown in Post Menu Page in each page.', 'billio' ),
				'before_widget' => '<div class="container billio-menuinfo">',
				'after_widget'  => '</div>',
			)
		);
	}

	add_action( 'widgets_init', 'register_foo_widget' );
endif;

if ( ! function_exists( 'remove_theme_header' ) ) :

	/**
	 * Remove theme header.
	 */
	function remove_theme_header() {
		remove_action( 'billio_header', 'billio_header_template', 10, 2 );
	}

	if ( billio_plugin_setting( 'billio-navigation-activate-option' ) === 'on' ) {
		add_action( 'billio_header', 'remove_theme_header', 9 );
	}
endif;

if ( ! function_exists( 'replace_billio_header_template' ) ) :

	/**
	 * Replace existing header.
	 */
	function replace_billio_header_template() {
		include billio_PLUGIN_DIR . '/templates-parts/header/layout.php';
		include billio_PLUGIN_DIR . '/templates-parts/header/banner.php';
	}

	if ( billio_plugin_setting( 'billio-navigation-activate-option' ) === 'on' ) {
		add_action( 'billio_header', 'replace_billio_header_template', 10 );
	}
endif;

if ( ! function_exists( 'billio_navigation_body_class' ) ) :

	/**
	 * Billio navigation body class.
	 *
	 * @param array $classes Body Class.
	 */
	function billio_navigation_body_class( $classes ) {
		$classes[] = 'on' === get_theme_mod( 'billio-navigation-activate-option' ) ? 'billio-navigation-active' : '';

		return $classes;
	}

	add_filter( 'body_class', 'billio_navigation_body_class' );
endif;

if ( ! function_exists( 'billio_addon_primary_color' ) ) :

	/**
	 * Billio Primary Color.
	 *
	 * @param array  $css CSS.
	 * @param string $primary_color Primary Color.
	 */
	function billio_addon_primary_color( $css, $primary_color ) {
		$css[] = array(
			'element' => '.single-recruitments .career-detail .career-action-button .apply, .single-recruitments .career-detail .career-action-button .emailtoafriend',
			'rules'   => array(
				'background-color' => esc_attr( $primary_color . ' !important' ),
			),
		);

		$css[] = array(
			'element' => 'ul#menu-services',
			'rules'   => array(
				'border-top-color' => esc_attr( $primary_color . ' !important' ),
			),
		);

		$css[] = array(
			'element' => '#menu-services li a:hover',
			'rules'   => array(
				'color' => esc_attr( $primary_color ),
			),
		);

		$css[] = array(
			'element' => '#menu-services li.current-menu-item',
			'rules'   => array(
				'background-color' => esc_attr( $primary_color ),
			),
		);

		$css[] = array(
			'element' => '.eg-washingtone-075257-element-13-a .eg-washingtone-075257-element-13',
			'rules'   => array(
				'background-color' => esc_attr( $primary_color . '!important' ),
			),
		);

		$css[] = array(
			'element' => '.eg-washingtone-075257-element-10',
			'rules'   => array(
				'color' => esc_attr( $primary_color . '!important' ),
			),
		);

		return $css;
	}

	add_filter( 'billio_primary_color_filter', 'billio_addon_primary_color', 10, 2 );
endif;
