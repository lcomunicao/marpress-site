<?php
/**
 * Customizer option and render to frontend.
 *
 * @package billio Plugin
 */

require_once billio_PLUGIN_DIR . '/inc/simple-line-icons.php';

if ( ! function_exists( 'billio_plugin_customizer_settings' ) ) :

	/**
	 * This file config for customizer.
	 *
	 * @param object $wp_customize  WP Customize object.
	 */
	function billio_plugin_customizer_settings( $wp_customize ) {
		global $billio_customizer_settings, $billio_customizer_sections;

		if ( ! isset( $billio_customizer_settings ) || ! isset( $billio_customizer_sections ) ) {
			return;
		}

		// billio Navigation Bar.
		$billio_customizer_sections['billio-addon-menu-option'] = array(
			'title'    => esc_html__( 'Billio Navigation Bar', 'billio-plugins' ),
			'panel'    => '',
			'priority' => 21,
		);

		$billio_customizer_settings['billio-navigation-activate-option'] = array(
			'section'     => 'billio-addon-menu-option',
			'type'        => 'custom',
			'custom_type' => 'billio-switcher',
			'default'     => 'off',
			'label'       => esc_html__( 'Billio Navigation Bar', 'billio-plugins' ),
			'description' => esc_html__( 'Activate/Deactivate Billio Navigation Bar', 'billio-plugins' ),
			'transport'   => 'refresh',
			'choices'     => array(
				'off' => esc_html__( 'Off', 'billio-plugins' ),
				'on'  => esc_html__( 'On', 'billio-plugins' ),
			),
		);

		$billio_customizer_sections['billio-addon-option'] = array(
			'title'    => esc_html__( 'Billio Options', 'billio-plugins' ),
			'panel'    => '',
			'priority' => 22,
		);

		$billio_customizer_settings['billio-disable-breadcrumb'] = array(
			'section'     => 'billio-addon-option',
			'type'        => 'custom',
			'custom_type' => 'billio-switcher',
			'default'     => 'block',
			'label'       => esc_html__( 'Enable/Disable Breadcrumb', 'billio-plugins' ),
			'transport'   => 'refresh',
			'choices'     => array(
				'none'  => esc_html__( 'Off', 'billio-plugins' ),
				'block' => esc_html__( 'On', 'billio-plugins' ),
			),
		);

		$billio_customizer_settings['billio-recruitments-email-target'] = array(
			'section'     => 'billio-addon-option',
			'type'        => 'textarea',
			'transport'   => 'refresh',
			'default'     => '',
			'label'       => esc_html__( 'RECRUITMENTS OPTIONS', 'billio-plugins' ),
			'description' => esc_html__( 'Target email address for the application form. If your\'e using multiple email addresses separate them using comma', 'billio-plugins' ),
		);

		$billio_customizer_settings['billio-recruitments-confirmation-message'] = array(
			'section'     => 'billio-addon-option',
			'type'        => 'textarea',
			'transport'   => 'refresh',
			'default'     => '',
			'description' => esc_html__( 'Message that will be displayed when the application has been successfully sent', 'billio-plugins' ),
		);

	}

	add_action( 'customize_register', 'billio_plugin_customizer_settings' );
endif;

if ( ! function_exists( 'billio_render_settings' ) ) :

	/**
	 * This is function to retrieve and render customizer settings on frontend.
	 */
	function billio_render_settings() {
		$css = array();

		wp_enqueue_style( 'billio-plugins-style', billio_PLUGIN_URL . 'assets/css/plugins.min.css' );

		if ( has_custom_logo() ) {
			$css[] = array(
				'element' => '#header-ext .navbar-wrapper .logo-container a.custom-logo-link img',
				'rules'   => array(
					'max-height' => billio_plugin_setting( 'logo-height' ) . 'px',
				),
			);
		}

		if ( billio_plugin_setting( 'billio-addon-navbar-logo-background' ) ) {
			$css[] = array(
				'element' => '#header-ext .navbar-wrapper .logo-container',
				'rules'   => array(
					'background-color' => get_theme_mod( 'billio-addon-navbar-logo-background' ),
				),
			);
		}

		if ( billio_plugin_setting( 'billio-addon-menu-font-color' ) ) {
			$css[] = array(
				'element' => '.navbar-nav>.menu-item:not(#mobile_navigation)>a, .navbar-nav>.menu-item:not(#mobile_navigation)',
				'rules'   => array(
					'color' => get_theme_mod( 'billio-addon-menu-font-color' ) . '!important',
				),
			);
		}

		if ( billio_plugin_setting( 'billio-addon-background-color' ) ) {
			$css[] = array(
				'element' => '#page',
				'rules'   => array(
					'background-color' => get_theme_mod( 'billio-addon-background-color' ) . '!important',
				),
			);
		}

		if ( billio_plugin_setting( 'billio-addon-navigation-bar-background-color' ) ) {
			$css[] = array(
				'element' => '#header-ext .navbar-wrapper .navigation-container .navigation-item, #navbardesktop',
				'rules'   => array(
					'background-color' => get_theme_mod( 'billio-addon-navigation-bar-background-color' ) . '!important',
				),
			);
		}

		if ( billio_plugin_setting( 'billio-addon-navigation-separator' ) === 'show' ) {
			$css[] = array(
				'element' => '#header-ext .navbar-wrapper .navigation-container .navigation-button-container .navigation-button',
				'rules'   => array(
					'border-bottom' => billio_plugin_setting( 'billio-addon-navigation-separator-color' ) . ' 1px solid',
				),
			);
		} else {
			$css[] = array(
				'element' => '#header-ext .navbar-wrapper .navigation-container .navigation-button-container .navigation-button',
				'rules'   => array(
					'border-bottom' => 'none',
				),
			);
		}

		if ( billio_plugin_setting( 'billio-addon-navigation-info-background-color' ) ) {
			$css[] = array(
				'element' => '#header-ext .navbar-wrapper .navigation-container .navigation-button-container',
				'rules'   => array(
					'background-color' => billio_plugin_setting( 'billio-addon-navigation-info-background-color' ),
				),
			);
		}

		if ( billio_plugin_setting( 'billio-addon-navigation-info-icon-color' ) ) {
			$css[] = array(
				'element' => '#header-ext .navbar-wrapper .navigation-container .navigation-button-container .navigation-button .navigation-button-item i::before',
				'rules'   => array(
					'color' => billio_plugin_setting( 'billio-addon-navigation-info-icon-color' ),
				),
			);
		}

		if ( billio_plugin_setting( 'billio-disable-breadcrumb' ) ) {
			$css[] = array(
				'element' => '#page>.breadcrumbs',
				'rules'   => array(
					'display' => billio_plugin_setting( 'billio-disable-breadcrumb' ),
				),
			);
		}

		if ( billio_plugin_setting( 'billio-addon-navigation-info-text-color' ) ) {
			$css[] = array(
				'element' => '#header-ext .navbar-wrapper .navigation-container .navigation-button-container .navigation-button .navigation-button-item .text-box .navigation-text',
				'rules'   => array(
					'color' => billio_plugin_setting( 'billio-addon-navigation-info-text-color' ),
				),
			);

			$css[] = array(
				'element' => '#header-ext .navbar-wrapper .navigation-container .navigation-button-container .navigation-button .navigation-button-item .text-box .navigation-label',
				'rules'   => array(
					'color' => billio_plugin_setting( 'billio-addon-navigation-info-text-color' ),
				),
			);
		}

		if ( has_header_image() ) {
			$css[] = array(
				'element' => '#header-image-bg',
				'rules'   => array(
					'background-image' => 'url(' . get_header_image() . ')',
				),
			);

			$css[] = array(
				'element' => '.breadcrumbs',
				'rules'   => array(
					'margin-top' => '0',
				),
			);
		} else {
			$css[] = array(
				'element' => '.breadcrumbs',
				'rules'   => array(
					'margin-top' => '0',
				),
			);

			$css[] = array(
				'element' => '#header-image #header-image-none',
				'rules'   => array(
					'background-color' => '#eeeeee',
				),
			);

			$css[] = array(
				'element' => '#header-image #header-image-none',
				'rules'   => array(
					'display' => 'block',
				),
			);

			$css[] = array(
				'element' => '#header-image #header-image-none',
				'rules'   => array(
					'height' => '100px',
				),
			);
		}

		// Check if quadmenu  inactive.
		if ( ! class_exists( 'Quadmenu' ) ) {
			$css[] = array(
				'element' => '#header-ext .navbar-wrapper .navigation-container .navigation-item',
				'rules'   => array(
					'padding' => '24px',
				),
			);
		} else {
			$css[] = array(
				'element' => 'nav#quadmenu',
				'rules'   => array(
					'padding' => '0 24px',
				),
			);
		}

		// If it's not the same with default value.
		$topbar_home_background_color = billio_plugin_setting( 'billio-addon-top-bar-background-color' );
		if ( billio_plugin_setting( 'billio-addon-top-bar-background-color', true ) !== $topbar_home_background_color ) {
			$css[] = array(
				'element' => '.topbar-desktop.billio-plugin-topbar',
				'rules'   => array(
					'background-color' => $topbar_home_background_color,
				),
			);

			$css[] = array(
				'element' => '#header-image #header-image-none',
				'rules'   => array(
					'background-color' => $topbar_home_background_color,
				),
			);
		}

		$css_output = array();

		foreach ( $css as $_css ) {
			$css_output[] = $_css['element'] . '{';

			foreach ( $_css['rules'] as $rule => $props ) {
				$css_output[] = $rule . ':' . $props;
			}

			$css_output[] = '}';
		}

		wp_add_inline_style( 'billio-plugins-style', implode( '', $css_output ) );
	}

	add_action( 'wp_enqueue_scripts', 'billio_render_settings' );
endif;
