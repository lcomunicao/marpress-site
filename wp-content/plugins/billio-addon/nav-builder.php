<?php
/**
 * The template for displaying footer widgets or custom page container.
 *
 * @package Vast
 * @since 1.0.0
 */

$postid  = get_the_ID();
$navpage = billio_theme_setting( 'footer-content' );

if ( ! $navpage ) {
	return;
}

?>

<div id="billio_nav_page">
	<div class="row d-flex flex-wrap">
	<?php
		do_action( 'billio_display_nav_builder' );
	?>

	</div><!-- .row -->
</div><!-- #builder -->
