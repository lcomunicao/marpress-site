<?php
/**
 * Top Bar Layout
 *
 * @package billio
 */

if ( billio_theme_setting( 'topbar-switch' ) !== 'true' ) {
	return;
}
?>

<div id="topbar" class="topbar-desktop billio-plugin-topbar d-none d-lg-block">
	<div class="container">
		<div class="row">
			<div class="topbar-left col-12 col-sm-6">
<?php
				echo wp_kses_post( billio_theme_setting( 'topbar-left' ) );
?>
			</div>
			<div class="topbar-right col-12 col-sm-6 text-right">
<?php
				echo wp_kses_post( billio_theme_setting( 'topbar-right' ) );
?>
			</div>
		</div>
	</div>
</div>
