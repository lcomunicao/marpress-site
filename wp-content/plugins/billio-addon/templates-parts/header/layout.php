<?php
/**
 * Main Header Layout
 *
 * @package Vast
 */

?>

<header id="header">
	<?php require billio_PLUGIN_DIR . '/templates-parts/header/navbar/topbar.php'; ?>
	<?php require billio_PLUGIN_DIR . '/templates-parts/header/navbar/layout.php'; ?>
	<?php if ( is_active_sidebar( 'nav-infobox' ) ) : ?>
	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'nav-infobox' ); ?>
	</div><!-- #primary-sidebar -->
	<?php endif; ?>
</header>
