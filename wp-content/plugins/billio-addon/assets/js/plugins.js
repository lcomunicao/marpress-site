(function () {
'use strict';

var breadcrumbsOption = (function ($) {
  $('#as_customizer_breadcrumbs').on('click', function () {
    $('#hide_breadcrumbs').prop('disabled', $(this).prop('checked'));
  });
});

(function ($) {
  $(document).ready(function () {
    breadcrumbsOption($);
  });
})(jQuery);

}());
