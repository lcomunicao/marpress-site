<?php
/**
 * Plugin Name: Billio Addon
 * Plugin URI:  https://www.detheme.com
 * Description: Plugin of billio, it contains menu, custom page and customizer option.
 * Version:     1.0.2
 * Author:      deTheme
 * Author URI:  https://www.detheme.com
 * Copyright:   2019 Billio Plugins (https://www.detheme.com)
 * Text Domain: billio-plugins
 *
 * @package billio plugin
 */

defined( 'ABSPATH' ) || die( 'What are you doing here ?' );

define( 'billio_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'billio_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

require 'inc/helpers.php';
require 'inc/hooks.php';
require 'inc/customizer.php';
require 'inc/custom-post/service/class-billio-plugins-cpt-services.php';
require 'inc/custom-post/recruitments/class-billio-plugins-cpt-recruitments.php';
require 'inc/custom-post/reports/class-billio-plugins-cpt-reports.php';
require 'inc/custom-post/project/class-billio-plugins-cpt-project.php';
require 'inc/lib/class-billio-plugins-widget.php';
/**
 * Class instance init.
 */
$billio_cpt_services     = new billio_Plugins_Cpt_Services();
$billio_cpt_recruitments = new billio_Plugins_Cpt_Recruitments();
$billio_cpt_reports      = new billio_Plugins_Cpt_Reports();
$billio_cpt_project      = new billio_Plugins_Cpt_Project();
$billio_widget           = new Foo_Widget();

/**
 * 1 Click Support.
 */
require_once billio_PLUGIN_DIR . 'inc/1-click-support/1-click-support.php';

$theme      = wp_get_theme();
$theme_name = 'Billio';

if ( $theme_name == $theme->name || $theme_name == $theme->parent_theme ) {
	include_once billio_PLUGIN_DIR . 'inc/1-click-support/show-beacon.php';
}

